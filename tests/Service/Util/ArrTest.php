<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Util;

use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Service\Util\Arr;
use ValueError;

class ArrTest extends TestCase
{
    /**
     * @dataProvider positiveGetCasesProvider
     *
     * @param mixed $expected
     */
    public function testGetPositive(array $array, string $key, $expected): void
    {
        $result = Arr::get($array, $key);

        self::assertFalse($result->isEmpty());
        self::assertEquals($expected, $result->get());
    }

    public static function positiveGetCasesProvider(): array
    {
        return [
            [['foo' => 123], 'foo', 123],
            [['foo' => ['abc']], 'foo', ['abc']],
            [['foo' => ['foo' => '123', 'bar' => 'abc']], 'foo.bar', 'abc'],
        ];
    }

    /**
     * @dataProvider negativeGetCasesProvider
     */
    public function testGetNegative(array $array, string $key): void
    {
        self::assertTrue(Arr::get($array, $key)->isEmpty());
    }

    public static function negativeGetCasesProvider(): array
    {
        return [
            [[], 'foo'],
            [['foo' => null], 'foo'],
            [[], 'foo.bar'],
            [['foo' => []], 'foo.bar'],
        ];
    }

    /**
     * @dataProvider failingGetCasesProvider
     */
    public function testGetFail(array $array, string $key): void
    {
        $this->expectException(ValueError::class);

        Arr::get($array, $key);
    }

    public static function failingGetCasesProvider(): array
    {
        return [
            [['foo' => []], 'foo.'],
            [['foo' => []], ''],
        ];
    }

    /**
     * @dataProvider positiveSetCasesProvider
     *
     * @param mixed $value
     */
    public function testSetPositive(array $array, string $key, $value, array $expected): void
    {
        self::assertEquals($expected, Arr::set($array, $key, $value));
    }

    public static function positiveSetCasesProvider(): array
    {
        return [
            [[], 'foo', 'bar', ['foo' => 'bar']],
            [['foo' => 123], 'foo', 'bar', ['foo' => 'bar']],
            [['foo' => []], 'foo.bar', 'baz', ['foo' => ['bar' => 'baz']]],
            [['foo' => null], 'foo.bar', 'baz', ['foo' => ['bar' => 'baz']]],
            [['foo' => ['asd' => 123], 'fooo' => 1234], 'foo.bar', 'baz', ['foo' => ['asd' => 123, 'bar' => 'baz'], 'fooo' => 1234]],
            [['foo' => ['asd' => 123]], 'foo.bar', ['baz' => 12345], ['foo' => ['asd' => 123, 'bar' => ['baz' => 12345]]]],
            [['foo' => 'abc'], 'foo.bar', 'bar', ['foo' => ['bar' => 'bar']]],
            [['foo' => ['bar' => false], 123], 'foo.bar.baz', 'qwerty', ['foo' => ['bar' => ['baz' => 'qwerty']], 123]],
        ];
    }

    /**
     * @dataProvider failingSetCasesProvider
     *
     * @param mixed $value
     */
    public function testSetFail(array $array, string $key, $value): void
    {
        $this->expectException(ValueError::class);

        Arr::set($array, $key, $value)->isEmpty();
    }

    public static function failingSetCasesProvider(): array
    {
        return [
            [['foo' => null], '', '123'],
            [['foo' => []], 'foo.', ''],
            [['foo' => []], '', 'baz'],
        ];
    }
}
