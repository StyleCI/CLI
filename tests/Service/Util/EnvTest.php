<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Util;

use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Service\Util\Env;

class EnvTest extends TestCase
{
    public function testGetNotFound(): void
    {
        self::assertTrue(Env::get('FOO_BAR_BAZ1')->isEmpty());
        self::assertTrue(Env::getFiltered('FOO_BAR_BAZ1')->isEmpty());
    }

    public function testGetBadType(): void
    {
        $_SERVER['FOO_BAR_BAZ2'] = 123;
        self::assertTrue(Env::get('FOO_BAR_BAZ2')->isEmpty());
        self::assertTrue(Env::getFiltered('FOO_BAR_BAZ2')->isEmpty());
    }

    public function testGetNull(): void
    {
        $_SERVER['FOO_BAR_BAZ3'] = null;
        self::assertTrue(Env::get('FOO_BAR_BAZ3')->isEmpty());
        self::assertTrue(Env::getFiltered('FOO_BAR_BAZ3')->isEmpty());
    }

    public function testGetEmptyString(): void
    {
        $_SERVER['FOO_BAR_BAZ4'] = '';
        self::assertFalse(Env::get('FOO_BAR_BAZ4')->isEmpty());
        self::assertSame('', Env::get('FOO_BAR_BAZ4')->get());
        self::assertTrue(Env::getFiltered('FOO_BAR_BAZ4')->isEmpty());
    }

    public function testGetZeroString(): void
    {
        $_SERVER['FOO_BAR_BAZ5'] = '0';
        self::assertFalse(Env::get('FOO_BAR_BAZ5')->isEmpty());
        self::assertSame('0', Env::get('FOO_BAR_BAZ5')->get());
        self::assertFalse(Env::getFiltered('FOO_BAR_BAZ5')->isEmpty());
        self::assertSame('0', Env::getFiltered('FOO_BAR_BAZ5')->get());
    }

    public function testGetOneString(): void
    {
        $_SERVER['FOO_BAR_BAZ6'] = '1';
        self::assertFalse(Env::get('FOO_BAR_BAZ6')->isEmpty());
        self::assertSame('1', Env::get('FOO_BAR_BAZ6')->get());
        self::assertFalse(Env::getFiltered('FOO_BAR_BAZ6')->isEmpty());
        self::assertSame('1', Env::getFiltered('FOO_BAR_BAZ6')->get());
    }

    public function testGetStandardString(): void
    {
        $_SERVER['FOO_BAR_BAZ7'] = 'qwertyuiop';
        self::assertFalse(Env::get('FOO_BAR_BAZ7')->isEmpty());
        self::assertSame('qwertyuiop', Env::get('FOO_BAR_BAZ7')->get());
        self::assertFalse(Env::getFiltered('FOO_BAR_BAZ7')->isEmpty());
        self::assertSame('qwertyuiop', Env::getFiltered('FOO_BAR_BAZ7')->get());
    }
}
