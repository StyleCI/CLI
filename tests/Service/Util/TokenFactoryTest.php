<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Util;

use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Service\Util\TokenFactory;

class TokenFactoryTest extends TestCase
{
    /**
     * @dataProvider negativeTokenStringProvider
     */
    public function testCreateNegative(array $raw): void
    {
        self::assertTrue(TokenFactory::create($raw)->isEmpty());
    }

    public static function negativeTokenStringProvider(): array
    {
        return [
            [[]],
            [['token' => []]],
            [['token' => 123]],
            [['token' => '']],
            [['tOken' => 'qwertyuiop']],
        ];
    }

    /**
     * @dataProvider positiveTokenStringProvider
     */
    public function testCreatePositive(array $raw, string $value): void
    {
        $token = TokenFactory::create($raw);
        self::assertFalse($token->isEmpty());
        self::assertSame($value, $token->get()->getValue());
    }

    public static function positiveTokenStringProvider(): array
    {
        return [
            [['token' => 'foo'], 'foo'],
            [['token' => 'github/qwertyuiop'], 'github/qwertyuiop'],
        ];
    }
}
