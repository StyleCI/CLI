<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Util;

use GrahamCampbell\TestBenchCore\MockeryTrait;
use GuzzleHttp\Psr7\Utils;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use StyleCI\CLI\Model\Analysis;
use StyleCI\CLI\Model\Status;
use StyleCI\CLI\Model\Token;
use StyleCI\CLI\Service\Util\ResponseParser;

class ResponseParserTest extends TestCase
{
    use MockeryTrait;

    /**
     * @dataProvider negativeTokenResponseProvider
     */
    public function testParseTokenNegative(string $raw): void
    {
        self::assertTrue(ResponseParser::parseToken(self::createResponse($raw))->isEmpty());
    }

    public static function negativeTokenResponseProvider(): array
    {
        return [
            [''],
            ['{"successsss":{"message":"Hello","token":null}}'],
            ['{"success":{"message":"Analysis successfully queued.","token":null}}'],
            ['{"success":{"message":"Analysis successfully queued.","token":{}}}'],
            ['{"success":{"message":"Analysis successfully queued.","token":""}}'],
        ];
    }

    /**
     * @dataProvider positiveTokenResponseProvider
     */
    public function testParseTokenPositive(string $raw, string $value): void
    {
        $token = ResponseParser::parseToken(self::createResponse($raw));

        self::assertFalse($token->isEmpty());
        self::assertInstanceOf(Token::class, $token->get());
        self::assertSame($value, $token->get()->getValue());
    }

    public static function positiveTokenResponseProvider(): array
    {
        return [
            ['{"success":{"message":"Analysis successfully queued.","token":"gitlab\/52566c07-a23a-420b-81df-d368a2aa6bc2"}}', 'gitlab/52566c07-a23a-420b-81df-d368a2aa6bc2'],
        ];
    }

    /**
     * @dataProvider negativeAnalysisResponseProvider
     */
    public function testParseAnalysisnNegative(string $raw): void
    {
        self::assertTrue(ResponseParser::parseAnalysis(self::createResponse($raw))->isEmpty());
    }

    public static function negativeAnalysisResponseProvider(): array
    {
        return [
            [''],
            ['{"successsss":{"message":"Hello","token":null}}'],
            ['{"success":{"message":"The analysis is pending.","analysis":null}}'],
            ['{"success":{"message":"The analysis is pending.","analysis":{}}}'],
            ['{"success":{"message":"The analysis is pending.","analysis":""}}'],
            ['{"success":{"message":"The analysis is pending.","analysis":{"url":"https:\/\/gitlab.styleci.io\/analyses\/RmV0Zd","status":"QWERTY"}}}'],
        ];
    }

    /**
     * @dataProvider positiveAnalysisResponseProvider
     */
    public function testParseAnalysisPositive(string $raw, int $status, string $url, ?string $diff, array $errors): void
    {
        $analysis = ResponseParser::parseAnalysis(self::createResponse($raw));

        self::assertFalse($analysis->isEmpty());
        self::assertInstanceOf(Analysis::class, $analysis->get());
        self::assertSame($status, $analysis->get()->getStatus()->getValue());
        self::assertSame($url, $analysis->get()->getUrl()->getValue());

        if (null === $diff) {
            self::assertTrue($analysis->get()->getPatch()->isEmpty());
        } else {
            self::assertSame($diff, $analysis->get()->getPatch()->get()->getValue());
        }

        $messages = $analysis->get()->getMessages();
        self::assertCount(\count($errors), $messages);
        foreach ($errors as $index => $error) {
            self::assertSame($error[0], $messages[$index]->getTitle());
            self::assertSame($error[1], $messages[$index]->getBody());
        }
    }

    public static function positiveAnalysisResponseProvider(): array
    {
        return [
            [
                '{"success":{"message":"The analysis is pending.","analysis":{"url":"https:\/\/gitlab.styleci.io\/analyses\/RmV0Zd","status":"pending"}}}',
                Status::PENDING,
                'https://gitlab.styleci.io/analyses/RmV0Zd',
                null,
                [],
            ],
            [
                '{"success":{"message":"The analysis is running.","analysis":{"url":"https:\/\/gitlab.styleci.io\/analyses\/RmV0Zd","status":"running"}}}',
                Status::RUNNING,
                'https://gitlab.styleci.io/analyses/RmV0Zd',
                null,
                [],
            ],
            [
                '{"success":{"message":"The analysis has passed.","analysis":{"url":"https:\/\/gitlab.styleci.io\/analyses\/RmV0Zd","status":"passed"}}}',
                Status::PASSED,
                'https://gitlab.styleci.io/analyses/RmV0Zd',
                null,
                [],
            ],
            [
                '{"success":{"message":"Issues have been identified.","analysis":{"url":"https:\/\/gitlab.styleci.io\/analyses\/2WPwnR","status":"cs_issues","changes":1,"diff":"ZGlmZiAtLWdpdCBhL3Rlc3RzL1N1cHBvcnQvUmVzcG9uc2VQYXJzZXJUZXN0LnBocCBiL3Rlc3RzL1N1cHBvcnQvUmVzcG9uc2VQYXJzZXJUZXN0LnBocAppbmRleCA4NmMzMDk1MjZhNzc3ZTE0OWQ3NzU5NDkzYTIwOWQ2NGUzMjMwZThkLi5jNGJkN2MyMGE5MjRlNWVhMGMzY2EzYjQ4ZDY1MmNlNDM0ODk5MzcyIDEwMDY0NAotLS0gYS90ZXN0cy9TdXBwb3J0L1Jlc3BvbnNlUGFyc2VyVGVzdC5waHAKKysrIGIvdGVzdHMvU3VwcG9ydC9SZXNwb25zZVBhcnNlclRlc3QucGhwCkBAIC05NSw3ICs5NSw2IEBAIGNsYXNzIFJlc3BvbnNlUGFyc2VyVGVzdCBleHRlbmRzIFRlc3RDYXNlCiAgICAgICAgIHNlbGY6OmFzc2VydFNhbWUoJHVybCwgJGFuYWx5c2lzLT5nZXQoKS0+Z2V0VXJsKCktPmdldFZhbHVlKCkpOwogICAgIH0KIAotCiAgICAgcHVibGljIHN0YXRpYyBmdW5jdGlvbiBwb3NpdGl2ZUFuYWx5c2lzUmVzcG9uc2VQcm92aWRlcigpOiBhcnJheQogICAgIHsKICAgICAgICAgcmV0dXJuIFsK"}}}',
                Status::CS_ISSUES,
                'https://gitlab.styleci.io/analyses/2WPwnR',
                \file_get_contents(__DIR__.'/analysis-diff.txt'),
                [],
            ],
            [
                '{"success":{"message":"Issues have been identified.","analysis":{"url":"https:\/\/gitlab.styleci.io\/analyses\/RrVQgR","status":"syntax_issues","messages":[{"title":"Syntax Error - tests\/Support\/ResponseParserTest.php","body":"Unexpected \':\', expecting variable (T_VARIABLE) on line 103."}]}}}',
                Status::SYNTAX_ISSUES,
                'https://gitlab.styleci.io/analyses/RrVQgR',
                null,
                [['Syntax Error - tests/Support/ResponseParserTest.php', 'Unexpected \':\', expecting variable (T_VARIABLE) on line 103.']],
            ],
            [
                '{"success":{"message":"Issues have been identified.","analysis":{"url":"https:\/\/gitlab.styleci.io\/analyses\/dyVkZO","status":"both_issues","messages":[{"title":"Syntax Error - tests\/Support\/ResponseParserTest.php","body":"Unexpected \':\', expecting variable (T_VARIABLE) on line 113."}],"changes":1,"diff":"ZGlmZiAtLWdpdCBhL3Rlc3RzL1N1cHBvcnQvUmVzcG9uc2VQYXJzZXJUZXN0LnBocCBiL3Rlc3RzL1N1cHBvcnQvUmVzcG9uc2VQYXJzZXJUZXN0LnBocAppbmRleCA4NmMzMDk1MjZhNzc3ZTE0OWQ3NzU5NDkzYTIwOWQ2NGUzMjMwZThkLi5jNGJkN2MyMGE5MjRlNWVhMGMzY2EzYjQ4ZDY1MmNlNDM0ODk5MzcyIDEwMDY0NAotLS0gYS90ZXN0cy9TdXBwb3J0L1Jlc3BvbnNlUGFyc2VyVGVzdC5waHAKKysrIGIvdGVzdHMvU3VwcG9ydC9SZXNwb25zZVBhcnNlclRlc3QucGhwCkBAIC05NSw3ICs5NSw2IEBAIGNsYXNzIFJlc3BvbnNlUGFyc2VyVGVzdCBleHRlbmRzIFRlc3RDYXNlCiAgICAgICAgIHNlbGY6OmFzc2VydFNhbWUoJHVybCwgJGFuYWx5c2lzLT5nZXQoKS0+Z2V0VXJsKCktPmdldFZhbHVlKCkpOwogICAgIH0KIAotCiAgICAgcHVibGljIHN0YXRpYyBmdW5jdGlvbiBwb3NpdGl2ZUFuYWx5c2lzUmVzcG9uc2VQcm92aWRlcigpOiBhcnJheQogICAgIHsKICAgICAgICAgcmV0dXJuIFsK"}}}',
                Status::BOTH_ISSUES,
                'https://gitlab.styleci.io/analyses/dyVkZO',
                \file_get_contents(__DIR__.'/analysis-diff.txt'),
                [['Syntax Error - tests/Support/ResponseParserTest.php', 'Unexpected \':\', expecting variable (T_VARIABLE) on line 113.']],
            ],
            [
                '{"success":{"message":"The analysis was misconfigured.","analysis":{"url":"https:\/\/gitlab.styleci.io\/analyses\/RvV6aR","status":"config_error","messages":[{"title":"Config Issue","body":"The yaml must represent an array."}]}}}',
                Status::CONFIG_ERROR,
                'https://gitlab.styleci.io/analyses/RvV6aR',
                null,
                [['Config Issue', 'The yaml must represent an array.']],
            ],
        ];
    }

    private static function createResponse(string $body): ResponseInterface
    {
        $response = Mockery::mock(ResponseInterface::class);
        $response->shouldReceive('getBody')->once()->with()->andReturn(Utils::streamFor($body));

        return $response;
    }
}
