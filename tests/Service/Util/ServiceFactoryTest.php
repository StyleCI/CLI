<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Util;

use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Model\Host;
use StyleCI\CLI\Model\Service;
use StyleCI\CLI\Service\Util\ServiceFactory;

class ServiceFactoryTest extends TestCase
{
    /**
     * @dataProvider negativeServiceStringProvider
     */
    public function testCreateNegative(string $raw): void
    {
        self::assertTrue(ServiceFactory::create(Host::create($raw))->isEmpty());
    }

    public static function negativeServiceStringProvider(): array
    {
        return [
            ['123'],
            ['example.com'],
            ['gitlab.org'],
            ['https://github.com'],
            ['  bitbucket.org'],
        ];
    }

    /**
     * @dataProvider positiveServiceStringProvider
     */
    public function testCreatePositive(string $raw, string $value): void
    {
        $service = ServiceFactory::create(Host::create($raw));
        self::assertFalse($service->isEmpty());
        self::assertSame($value, $service->get()->getValue());
    }

    public static function positiveServiceStringProvider(): array
    {
        return [
            ['bitbucket.org', Service::BITBUCKET],
            ['gitlab.com', Service::GITLAB],
            ['github.com', Service::GITHUB],
        ];
    }
}
