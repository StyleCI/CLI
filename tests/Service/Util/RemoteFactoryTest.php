<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Util;

use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Service\Util\RemoteFactory;

class RemoteFactoryTest extends TestCase
{
    /**
     * @dataProvider negativeRemoteStringProvider
     */
    public function testCreateNegative(string $raw): void
    {
        self::assertTrue(RemoteFactory::create($raw)->isEmpty());
    }

    public static function negativeRemoteStringProvider(): array
    {
        return [
            [''],
            ['123'],
            ['git@github.com/laravel/framework.git'],
        ];
    }

    /**
     * @dataProvider positiveRemoteStringProvider
     */
    public function testCreatePositive(string $raw, string $host, string $path): void
    {
        $remote = RemoteFactory::create($raw);
        self::assertFalse($remote->isEmpty());
        self::assertSame($host, $remote->get()->getHost()->getValue());
        self::assertSame($path, $remote->get()->getPath()->getValue());
    }

    public static function positiveRemoteStringProvider(): array
    {
        return [
            ['https://github.com/laravel/framework', 'github.com', 'laravel/framework'],
            ['https://github.com/laravel/framework.git', 'github.com', 'laravel/framework'],
            ['git@github.com:laravel/framework.git', 'github.com', 'laravel/framework'],
            ['git@github.com:laravel/framework', 'github.com', 'laravel/framework'],
            ['git@example.com:somerepo', 'example.com', 'somerepo'],
        ];
    }
}
