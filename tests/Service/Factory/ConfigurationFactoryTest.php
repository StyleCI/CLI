<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Factory;

use GrahamCampbell\TestBenchCore\MockeryTrait;
use Mockery;
use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Contract\Configuration;
use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Service\Factory\ConfigurationFactory;
use Symfony\Component\Console\Input\InputInterface;

class ConfigurationFactoryTest extends TestCase
{
    use MockeryTrait;

    public function testCreate(): void
    {
        $configuration = ConfigurationFactory::create(
            Filepath::create(__FILE__),
            Filepath::create(__DIR__),
            Mockery::mock(InputInterface::class)
        );

        self::assertInstanceOf(Configuration::class, $configuration);
    }

    public function testCreateWithLogger(): void
    {
        $configuration = ConfigurationFactory::create(
            Filepath::create(__FILE__),
            Filepath::create(__DIR__),
            Mockery::mock(InputInterface::class),
            Mockery::mock(Logger::class)
        );

        self::assertInstanceOf(Configuration::class, $configuration);
    }
}
