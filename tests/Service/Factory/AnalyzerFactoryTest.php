<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Factory;

use GrahamCampbell\TestBenchCore\MockeryTrait;
use Mockery;
use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Contract\Analyzer;
use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Model\Host;
use StyleCI\CLI\Model\UserAgent;
use StyleCI\CLI\Service\Factory\AnalyzerFactory;

class AnalyzerFactoryTest extends TestCase
{
    use MockeryTrait;

    public function testCreate(): void
    {
        $analyzer = AnalyzerFactory::create(
            Host::create('example.com'),
            UserAgent::create('curl/1.0')
        );

        self::assertInstanceOf(Analyzer::class, $analyzer);
    }

    public function testCreateWithLogger(): void
    {
        $analyzer = AnalyzerFactory::create(
            Host::create('example.com'),
            UserAgent::create('curl/1.0'),
            Mockery::mock(Logger::class)
        );

        self::assertInstanceOf(Analyzer::class, $analyzer);
    }
}
