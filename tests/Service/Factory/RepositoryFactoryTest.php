<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Factory;

use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Contract\Repository;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Service\Factory\RepositoryFactory;

class RepositoryFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $repository = RepositoryFactory::create(
            Filepath::create(__DIR__)
        );

        self::assertInstanceOf(Repository::class, $repository);
    }

    public function testCreateWithGitBinary(): void
    {
        $repository = RepositoryFactory::create(
            Filepath::create(__DIR__),
            Filepath::create('git')
        );

        self::assertInstanceOf(Repository::class, $repository);
    }
}
