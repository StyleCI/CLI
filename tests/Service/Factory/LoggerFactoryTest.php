<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service\Factory;

use GrahamCampbell\TestBenchCore\MockeryTrait;
use Mockery;
use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Service\Factory\LoggerFactory;
use Symfony\Component\Console\Output\OutputInterface;

class LoggerFactoryTest extends TestCase
{
    use MockeryTrait;

    public function testCreate(): void
    {
        $logger = LoggerFactory::create(
            Mockery::mock(OutputInterface::class)
        );

        self::assertInstanceOf(Logger::class, $logger);
    }
}
