<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service;

use GrahamCampbell\GitWrapper\Exception\GitException;
use GrahamCampbell\GitWrapper\GitWrapper;
use GrahamCampbell\TestBenchCore\MockeryTrait;
use Mockery;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use StyleCI\CLI\Contract\Repository;
use StyleCI\CLI\Model\Commit;
use StyleCI\CLI\Model\Diff;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Remote;
use StyleCI\CLI\Model\RemoteName;
use StyleCI\CLI\Model\Repo;
use StyleCI\CLI\Model\State;
use StyleCI\CLI\Service\GitRepository;

class GitRepositoryTest extends TestCase
{
    use MockeryTrait;

    public function testGetMainRemotePositive(): void
    {
        $mockGitWrapper = Mockery::mock(new GitWrapper());
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('remote get-url origin', __DIR__)
            ->andReturn("git@gitlab.com:StyleCI/CLI.git\n");

        $repository = self::createRepository($mockGitWrapper);

        $remote = $repository->getMainRemote(
            Repo::create(Filepath::create(__DIR__), RemoteName::create('origin'))
        );

        self::assertFalse($remote->isEmpty());
        self::assertInstanceOf(Remote::class, $remote->get());
        self::assertSame('gitlab.com', $remote->get()->getHost()->getValue());
        self::assertSame('StyleCI/CLI', $remote->get()->getPath()->getValue());
    }

    public function testGetMainRemoteNegative(): void
    {
        $mockGitWrapper = Mockery::mock(new GitWrapper());
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('remote get-url hello', __DIR__)
            ->andThrow(new GitException("fatal: No such remote 'hello'\n"));

        $repository = self::createRepository($mockGitWrapper);

        $remote = $repository->getMainRemote(
            Repo::create(Filepath::create(__DIR__), RemoteName::create('hello'))
        );

        self::assertTrue($remote->isEmpty());
    }

    public function testGetLatestRemoteStatePositive(): void
    {
        $mockGitWrapper = Mockery::mock(new GitWrapper());
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('log -200 --pretty=format:%H', __DIR__)
            ->andReturn(\file_get_contents(__DIR__.'/log-output.txt'));
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('branch -r --contains 4a63babcca01635102210a741fab992ffc73ba41', __DIR__)
            ->andReturn("\n");
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('branch -r --contains 9dbcfb1fc905e51246c90f0961ba6a8f037be367', __DIR__)
            ->andReturn("  origin/0.5\n  origin/HEAD -> origin/0.5\n");

        $repository = self::createRepository($mockGitWrapper);

        $remote = $repository->getLatestRemoteState(
            Repo::create(Filepath::create(__DIR__), RemoteName::create('origin'))
        );

        self::assertFalse($remote->isEmpty());
        self::assertInstanceOf(State::class, $remote->get());
        self::assertSame('0.5', $remote->get()->getBranch()->getValue());
        self::assertSame('9dbcfb1fc905e51246c90f0961ba6a8f037be367', $remote->get()->getCommit()->getValue());
    }

    public function testGetLatestRemoteStateNegative(): void
    {
        $mockGitWrapper = Mockery::mock(new GitWrapper());
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('log -200 --pretty=format:%H', __DIR__)
            ->andThrow(new GitException("fatal: your current branch 'main' does not have any commits yet\n"));

        $repository = self::createRepository($mockGitWrapper);

        $remote = $repository->getLatestRemoteState(
            Repo::create(Filepath::create(__DIR__), RemoteName::create('origin'))
        );

        self::assertTrue($remote->isEmpty());
    }

    public function testGenerateLocalDiffPositive(): void
    {
        $diffOutput = \file_get_contents(__DIR__.'/diff-output.txt');
        $changedFiles = [
            'app/Console/Command/AnalyzeCommand.php',
            'app/Contract/Logger.php',
            'app/Impl/Console/Logger.php',
            'app/Impl/Git/Repository.php',
            'tests/Console/ApplicationTest.php',
            'tests/Impl/Console/LoggerTest.php',
            'tests/Support/EnvTest.php',
            'tests/Support/RemoteFactoryTest.php',
            'tests/Support/ServiceFactoryTest.php',
            'tests/Support/TokenFactoryTest.php',
            'vendor-bin/phpunit/composer.json',
            'vendor-bin/phpunit/composer.lock',
            'vendor-bin/rector/composer.lock',
        ];

        $mockGitWrapper = Mockery::mock(new GitWrapper());
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('diff 9dbcfb1fc905e51246c90f0961ba6a8f037be367 --src-prefix=a/ --dst-prefix=b/ --binary --minimal --ignore-submodules --no-ext-diff --no-textconv', __DIR__)
            ->andReturn($diffOutput);
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('diff 9dbcfb1fc905e51246c90f0961ba6a8f037be367 --name-only --diff-filter=d --ignore-submodules --no-ext-diff --no-textconv', __DIR__)
            ->andReturn(\sprintf("%s\n", \implode("\n", $changedFiles)));

        $repository = self::createRepository($mockGitWrapper);

        $diff = $repository->generateLocalDiff(
            Repo::create(Filepath::create(__DIR__), RemoteName::create('origin')),
            Commit::create('9dbcfb1fc905e51246c90f0961ba6a8f037be367')
        );

        self::assertFalse($diff->isEmpty());
        self::assertInstanceOf(Diff::class, $diff->get());
        self::assertSame($diffOutput, $diff->get()->getPatch()->getValue());
        self::assertSame($changedFiles, $diff->get()->getFiles());
    }

    public function testGenerateLocalDiffEmpty(): void
    {
        $mockGitWrapper = Mockery::mock(new GitWrapper());
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('diff 9dbcfb1fc905e51246c90f0961ba6a8f037be367 --src-prefix=a/ --dst-prefix=b/ --binary --minimal --ignore-submodules --no-ext-diff --no-textconv', __DIR__)
            ->andReturn('');

        $repository = self::createRepository($mockGitWrapper);

        $diff = $repository->generateLocalDiff(
            Repo::create(Filepath::create(__DIR__), RemoteName::create('origin')),
            Commit::create('9dbcfb1fc905e51246c90f0961ba6a8f037be367')
        );

        self::assertTrue($diff->isEmpty());
    }

    public function testGenerateLocalDiffNegative(): void
    {
        $mockGitWrapper = Mockery::mock(new GitWrapper());
        $mockGitWrapper->shouldReceive('git')->once()
            ->with('diff 9dbcfb1fc905e51246c90f0961ba6a8f037be367 --src-prefix=a/ --dst-prefix=b/ --binary --minimal --ignore-submodules --no-ext-diff --no-textconv', __DIR__)
            ->andThrow(new GitException("fatal: ambiguous argument '9dbcfb1fc905e51246c90f0961ba6a8f037be367': unknown revision or path not in the working tree.\n"));

        $repository = self::createRepository($mockGitWrapper);

        $diff = $repository->generateLocalDiff(
            Repo::create(Filepath::create(__DIR__), RemoteName::create('origin')),
            Commit::create('9dbcfb1fc905e51246c90f0961ba6a8f037be367')
        );

        self::assertTrue($diff->isEmpty());
    }

    private static function createRepository(object $mockGitWrapper): Repository
    {
        $repository = new GitRepository(new GitWrapper(), Filepath::create(__DIR__));

        $property = (new ReflectionClass(GitRepository::class))->getProperty('gitWrapper');
        $property->setAccessible(true);
        $property->setValue($repository, $mockGitWrapper);

        return $repository;
    }
}
