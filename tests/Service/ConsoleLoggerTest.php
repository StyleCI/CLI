<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Service;

use GrahamCampbell\TestBenchCore\MockeryTrait;
use Mockery;
use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Model\Entry;
use StyleCI\CLI\Service\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class ConsoleLoggerTest extends TestCase
{
    use MockeryTrait;

    public function testWriteNormal(): void
    {
        $output = Mockery::mock(OutputInterface::class);
        $output->shouldReceive('writeln')->once()->with('<info>[DEBUG]</info> Test 123.', OutputInterface::VERBOSITY_VERY_VERBOSE);

        $logger = new ConsoleLogger($output);
        self::assertInstanceOf(Logger::class, $logger);
        $logger->write(Entry::create('Test 123.'));
    }

    public function testWriteDebug(): void
    {
        $output = Mockery::mock(OutputInterface::class);
        $output->shouldReceive('writeln')->once()->with('<info>[DEBUG]</info> Hello there!', OutputInterface::VERBOSITY_DEBUG);
        $output->shouldReceive('writeln')->once()->with('', OutputInterface::VERBOSITY_DEBUG);

        $logger = new ConsoleLogger($output);
        self::assertInstanceOf(Logger::class, $logger);
        $logger->write(Entry::createDebug('Hello there!'));
    }
}
