<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tests\Console;

use PHPUnit\Framework\TestCase;
use StyleCI\CLI\Console\Application;
use Symfony\Component\Console\Application as SymfonyApplication;

class ApplicationTest extends TestCase
{
    public function testCreateApplication(): void
    {
        $app = new Application();

        self::assertInstanceOf(SymfonyApplication::class, $app);
        self::assertSame('StyleCI CLI', $app->getName());
    }
}
