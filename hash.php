<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

echo "SHA1:\n";
echo sprintf("%s\n\n", hash_file('sha1', 'bin/styleci.phar'));

echo "SHA256:\n";
echo sprintf("%s\n\n", hash_file('sha256', 'bin/styleci.phar'));

echo "SHA384:\n";
echo sprintf("%s\n\n", hash_file('sha384', 'bin/styleci.phar'));
