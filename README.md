# StyleCI CLI Tool

This is the source code for the [StyleCI CLI Tool](https://github.com/StyleCI/CLI).

## License

This tool is licensed under [The Apache License 2.0](LICENSE).
