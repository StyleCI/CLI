<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Console;

use StyleCI\CLI\Console\Command\AnalyzeCommand;
use StyleCI\CLI\Console\Command\ConfigCommand;
use Symfony\Component\Console\Application as SymfonyApplication;

final class Application extends SymfonyApplication
{
    /**
     * The application name.
     *
     * @var string
     */
    private const APP_NAME = 'StyleCI CLI';

    /**
     * The application version.
     *
     * @var string
     */
    private const APP_VERSION = '1.5.1';

    /**
     * The API host name.
     *
     * @var string
     */
    private const API_HOST_NAME = 'api.styleci.io';

    /**
     * The StyleCI config file.
     *
     * @var string
     */
    private const CONFIG_FILE = '.styleci/config.json';

    /**
     * Create a new StyleCI CLI application.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(self::APP_NAME, self::APP_VERSION);
        $configFilePath = self::createConfigFilepath();
        $this->add($command = self::createAnalyzeCommand($this->getHelp(), $configFilePath));
        $this->add(self::createConfigCommand($this->getHelp(), $configFilePath));
        $this->setDefaultCommand((string) $command->getName());
    }

    /**
     * Create the StyleCI config filepath.
     */
    private static function createConfigFilepath(): string
    {
        /** @var mixed */
        $homeDir = $_SERVER['HOME'] ?? \getenv('HOME');

        if (!\is_string($homeDir)) {
            $homeDir = '';
        }

        return \sprintf('%s/%s', \rtrim($homeDir, '/'), self::CONFIG_FILE);
    }

    /**
     * Create a new analyze command instance.
     */
    private static function createAnalyzeCommand(string $appHeader, string $configFilePath): AnalyzeCommand
    {
        return new AnalyzeCommand(
            $appHeader,
            self::API_HOST_NAME,
            self::createUserAgent(),
            $configFilePath,
            (string) \getcwd(),
            \sys_get_temp_dir()
        );
    }

    /**
     * Create the API user agent string.
     */
    private static function createUserAgent(): string
    {
        return \sprintf(
            '%s/%s (%s; PHP %s)',
            \strtolower(\str_replace(' ', '-', self::APP_NAME)),
            self::APP_VERSION,
            \PHP_OS_FAMILY,
            \explode('-', \PHP_VERSION, 2)[0]
        );
    }

    /**
     * Create a new config command instance.
     */
    private static function createConfigCommand(string $appHeader, string $configFilePath): ConfigCommand
    {
        return new ConfigCommand(
            $appHeader,
            $configFilePath
        );
    }
}
