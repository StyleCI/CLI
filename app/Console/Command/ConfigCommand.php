<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Console\Command;

use StyleCI\CLI\Console\Util\CommandExecutor;
use StyleCI\CLI\Contract\Configuration;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Key;
use StyleCI\CLI\Service\FileConfiguration;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ConfigCommand extends Command
{
    /**
     * The command name.
     *
     * @var string
     */
    private const COMMAND_NAME = 'config';

    /**
     * The command description.
     *
     * @var string
     */
    private const COMMAND_DESC = 'Set a StyleCI CLI Tool configuration entry';

    /**
     * The app header.
     *
     * @var string
     */
    private $appHeader;

    /**
     * The config directory.
     *
     * @var string
     */
    private $configFilepath;

    /**
     * Create a new analyze command instance.
     *
     * @return void
     */
    public function __construct(string $appHeader, string $configFilepath)
    {
        $this->appHeader = $appHeader;
        $this->configFilepath = $configFilepath;
        parent::__construct();
    }

    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription(self::COMMAND_DESC);
        $this->addArgument('key', InputArgument::REQUIRED);
        $this->addArgument('value', InputArgument::OPTIONAL);
    }

    /**
     * Executes the command.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        return CommandExecutor::execute($input, $output, $this->appHeader, function (InputInterface $input, OutputInterface $output): void {
            $configuration = new FileConfiguration(Filepath::create($this->configFilepath));
            /** @var string */
            $key = $input->getArgument('key');
            /** @var string */
            $value = $input->getArgument('value') ?? '';

            if ('' === $value) {
                self::clearConfig($configuration, $key);
                $output->writeln(\sprintf('<info>[INFO]</info>  Config entry "%s" cleared.', $key));
            } else {
                self::setConfig($configuration, $key, $value);
                $output->writeln(\sprintf('<info>[INFO]</info>  Config entry "%s: "%s" set.', $key, $value));
            }
        });
    }

    /**
     * Clear a configuration entry or throw an exception.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException|\RuntimeException
     */
    private static function clearConfig(Configuration $configuration, string $key): void
    {
        switch ($key) {
            case 'auth.bitbucket':
                $configuration->clearBitbucketKey();
                break;
            case 'auth.github':
                $configuration->clearGitHubKey();
                break;
            case 'auth.gitlab':
                $configuration->clearGitLabKey();
                break;
            case 'git.binary':
                $configuration->clearGitBinary();
                break;
            default:
                throw new InvalidArgumentException('Invalid configuration key provided. Must be one of "auth.bitbucket", "auth.github", "auth.gitlab", or "git.binary".');
        }
    }

    /**
     * Set a configuration entry or throw an exception.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException|\RuntimeException
     */
    private static function setConfig(Configuration $configuration, string $key, string $value): void
    {
        switch ($key) {
            case 'auth.bitbucket':
                $configuration->setBitbucketKey(self::createKey($value));
                break;
            case 'auth.github':
                $configuration->setGitHubKey(self::createKey($value));
                break;
            case 'auth.gitlab':
                $configuration->setGitLabKey(self::createKey($value));
                break;
            case 'git.binary':
                $configuration->setGitBinary(self::createFilepath($value));
                break;
            default:
                throw new InvalidArgumentException('Invalid configuration key provided. Must be one of "auth.bitbucket", "auth.github", "auth.gitlab", or "git.binary".');
        }
    }

    /**
     * Create a key from the given value or throw an exception.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    private static function createKey(string $value): Key
    {
        if (40 !== \strlen($value)) {
            throw new InvalidArgumentException('Invalid configuration value provided. The value must be 40 characters long.');
        }

        return Key::create($value);
    }

    /**
     * Create a filepath from the given value or throw an exception.
     *
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    private static function createFilepath(string $value): Filepath
    {
        if ('' === $value) {
            throw new InvalidArgumentException('Invalid configuration value provided. The value must be non-empty.');
        }

        return Filepath::create($value);
    }
}
