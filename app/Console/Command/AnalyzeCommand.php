<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Console\Command;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;
use StyleCI\CLI\Console\Util\CommandExecutor;
use StyleCI\CLI\Contract\Analyzer;
use StyleCI\CLI\Contract\Repository;
use StyleCI\CLI\Model\Analysis;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Changes;
use StyleCI\CLI\Model\Config;
use StyleCI\CLI\Model\DryRun;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Host;
use StyleCI\CLI\Model\Message;
use StyleCI\CLI\Model\OnlyChanged;
use StyleCI\CLI\Model\Patch;
use StyleCI\CLI\Model\RemoteName;
use StyleCI\CLI\Model\Repo;
use StyleCI\CLI\Model\Status;
use StyleCI\CLI\Model\Submission;
use StyleCI\CLI\Model\Token;
use StyleCI\CLI\Model\UserAgent;
use StyleCI\CLI\Service\Factory\AnalyzerFactory;
use StyleCI\CLI\Service\Factory\ConfigurationFactory;
use StyleCI\CLI\Service\Factory\LoggerFactory;
use StyleCI\CLI\Service\Factory\RepositoryFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

final class AnalyzeCommand extends Command
{
    /**
     * The command name.
     *
     * @var string
     */
    private const COMMAND_NAME = 'analyze';

    /**
     * The command description.
     *
     * @var string
     */
    private const COMMAND_DESC = 'Analyze a local copy of a repo enabled on StyleCI';

    /**
     * The app header.
     *
     * @var string
     */
    private $appHeader;

    /**
     * The API host name.
     *
     * @var string
     */
    private $apiHost;

    /**
     * The API user agent.
     *
     * @var string
     */
    private $apiUserAgent;

    /**
     * The config directory.
     *
     * @var string
     */
    private $configFilepath;

    /**
     * The working directory.
     *
     * @var string
     */
    private $workingDirectory;

    /**
     * The temporary directory.
     *
     * @var string
     */
    private $tempDirectory;

    /**
     * Create a new analyze command instance.
     *
     * @return void
     */
    public function __construct(
        string $appHeader,
        string $apiHost,
        string $apiUserAgent,
        string $configFilepath,
        string $workingDirectory,
        string $tempDirectory
    ) {
        $this->appHeader = $appHeader;
        $this->apiHost = $apiHost;
        $this->apiUserAgent = $apiUserAgent;
        $this->configFilepath = $configFilepath;
        $this->workingDirectory = $workingDirectory;
        $this->tempDirectory = $tempDirectory;
        parent::__construct();
    }

    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription(self::COMMAND_DESC);
        $this->addArgument('directory', InputArgument::OPTIONAL);
        $this->addOption('only-changed', 'c');
        $this->addOption('dry-run', 'd');
        $this->addOption('git-binary', 'g', InputOption::VALUE_REQUIRED);
        $this->addOption('remote-name', 'r', InputOption::VALUE_REQUIRED);
    }

    /**
     * Executes the command.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        return CommandExecutor::execute($input, $output, $this->appHeader, function (InputInterface $input, OutputInterface $output): void {
            $output->writeln(\sprintf('<info>[DEBUG]</info> Using the config filepath "%s".', $this->configFilepath), OutputInterface::VERBOSITY_VERY_VERBOSE);

            $logger = $output->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE ? LoggerFactory::create($output) : null;
            $configuration = ConfigurationFactory::create(
                Filepath::create($this->configFilepath),
                Filepath::create($this->workingDirectory),
                $input,
                $logger
            );

            $auth = $configuration->getAuthConfig();
            $localConfig = $configuration->getLocalConfig();

            $repo = Repo::create(
                self::getRepoDirectory($localConfig, Filepath::create($this->workingDirectory)),
                $localConfig->getRemoteName()->getOrElse(RemoteName::createDefault())
            );

            $analyzer = AnalyzerFactory::create(
                Host::create($this->apiHost),
                UserAgent::create($this->apiUserAgent),
                $logger
            );

            $repository = RepositoryFactory::create(
                Filepath::create($this->tempDirectory),
                $localConfig->getGitBinary()->getOrElse(null)
            );

            $onlyChanged = $localConfig->getOnlyChanged()->getOrElse(OnlyChanged::createDefault())->getValue();
            $dryRun = $localConfig->getDryRun()->getOrElse(DryRun::createDefault())->getValue();

            self::triggerAnalysis($output, $analyzer, $repository, $auth, $repo, $onlyChanged)
                ->map(function (Submission $submission) use ($output, $analyzer, $repository, $repo, $dryRun): void {
                    $analysis = self::fetchAnalysis($output, $analyzer, $submission);

                    self::processAnalysis($output, $repository, $analysis, $repo, $dryRun);
                });
        });
    }

    /**
     * Get the repo directory.
     *
     * @throws \RuntimeException
     */
    private static function getRepoDirectory(Config $localConfig, Filepath $workingDirectory): Filepath
    {
        $repoDirectory = $localConfig->getRepoDirectory()->getOrElse($workingDirectory)->getValue();

        $realDirectory = \realpath($repoDirectory);

        if (false === $realDirectory) {
            throw new RuntimeException(\sprintf('The repository directory "%s" does not exist.', $repoDirectory));
        }

        if (!\is_dir($realDirectory)) {
            throw new RuntimeException(\sprintf('The repository directory "%s" does not exist.', $realDirectory));
        }

        if (!\is_dir(\sprintf('%s/.git', $realDirectory))) {
            throw new RuntimeException(\sprintf('The directory "%s" is not a git repository.', $realDirectory));
        }

        return Filepath::create($realDirectory);
    }

    /**
     * Trigger an analysis on the given repo, returning a status token.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Submission>
     */
    private static function triggerAnalysis(
        OutputInterface $output,
        Analyzer $analyzer,
        Repository $repository,
        Auth $auth,
        Repo $repo,
        bool $onlyChanged
    ): Option {
        $remote = $repository->getMainRemote($repo)
            ->getOrCall(function () use ($repo): void {
                throw new RuntimeException(\sprintf('Unable read repository remote. Please make sure you are analyzing the root directory of a git repository with a remote named "%s".', $repo->getRemoteName()->getValue()));
            });

        $output->writeln(\sprintf('<info>[INFO]</info>  Detected %s repo %s.', $remote->getHost()->getValue(), $remote->getPath()->getValue()));

        $state = $repository->getLatestRemoteState($repo)
            ->getOrCall(function (): void {
                throw new RuntimeException('Unable to find an upstream remote state. Please make sure at least one of the last 200 commits in the current branch has been pushed to the server.');
            });

        $output->writeln(\sprintf('<info>[DEBUG]</info> Chosen the remote known state %s on branch %s.', $state->getCommit()->getValue(), $state->getBranch()->getValue()), OutputInterface::VERBOSITY_VERY_VERBOSE);

        $diff = $repository->generateLocalDiff($repo, $state->getCommit());

        if ($diff->isDefined()) {
            $output->writeln('<info>[DEBUG]</info> We detected changes between the current state and the remote known state.', OutputInterface::VERBOSITY_VERY_VERBOSE);
        } else {
            $output->writeln('<info>[DEBUG]</info> We did not detect any changes between the current state and the remote known state.', OutputInterface::VERBOSITY_VERY_VERBOSE);
        }

        if ($onlyChanged && ($diff->isEmpty() || 0 === \count($diff->get()->getFiles()))) {
            $output->writeln('<info>[INFO]</info>  No changes detected, skipping analysis.');

            return None::create();
        }

        $output->writeln('<info>[INFO]</info>  Submitting local analysis request to StyleCI.');

        $submission = $analyzer->submitForAnalysis($auth, $remote, $state, $diff->getOrElse(null), $onlyChanged)
            ->mapError(function (string $error) use ($remote): void {
                switch ($error) {
                    case Analyzer::NOAUTH_ERROR:
                        throw new RuntimeException('The required API key was not set. Please either run "styleci config auth.bitbucket <your-api-key>", "styleci config auth.github <your-api-key>" or "styleci config auth.gitlab <your-api-key>", or set one of the following environment variables: STYLECI_BITBUCKET_API_KEY, STYLECI_GITHUB_API_KEY, STYLECI_GITLAB_API_KEY.');
                    case Analyzer::UNAUTH_ERROR:
                        throw new RuntimeException('StyleCI has rejected the analysis request due to an invalid API key. Your API can be replaced either by running "styleci config auth.bitbucket <your-api-key>", "styleci config auth.github <your-api-key>" or "styleci config auth.gitlab <your-api-key>", or by setting one of the following environment variables: STYLECI_BITBUCKET_API_KEY, STYLECI_GITHUB_API_KEY, STYLECI_GITLAB_API_KEY.');
                    case Analyzer::BLOCK_ERROR:
                        throw new RuntimeException('StyleCI has rejected the analysis request either because your IP address has been blocked or because StyleCI is not available in your country. Please contact support for a resolution.');
                    case Analyzer::RATE_ERROR:
                        throw new RuntimeException('StyleCI has rejected the analysis request because your IP address has been rate limited. Please retry the analysis in around one minute\'s time.');
                    case Analyzer::LARGE_ERROR:
                        throw new RuntimeException('StyleCI has rejected the analysis request because the payload was too large. Please check there aren\'t any additional tracked files accidentally in your working directory.');
                    case Analyzer::UPGRADE_ERROR:
                        throw new RuntimeException('StyleCI has rejected the analysis request because this version of the CLI tool is too old. Please upgrade to the latest version of the CLI tool.');
                    case Analyzer::SUSPENDED_ERROR:
                        $path = $remote->getPath()->getValue();

                        throw new RuntimeException(\sprintf('StyleCI has rejected the analysis request because the "%s" account is suspended. Please contact support for a resolution.', \explode('/', $path)[0]));
                    case Analyzer::PRO_ERROR:
                        $path = $remote->getPath()->getValue();

                        throw new RuntimeException(\sprintf('StyleCI has rejected the analysis request because the "%s" account is not subscribed to plan with StyleCI Pro features. Please upgrade plans on the web interface or contact support for more information.', \explode('/', $path)[0]));
                    case Analyzer::DENIED_ERROR:
                        $path = $remote->getPath()->getValue();

                        throw new RuntimeException(\sprintf('StyleCI has rejected the analysis request because you are not permitted to run analyses for the "%s" repo.', $path));
                    case Analyzer::ANALYSIS_ERROR:
                        $path = $remote->getPath()->getValue();

                        throw new RuntimeException(\sprintf('StyleCI has rejected the analysis request because the "%s" repo is not enabled on StyleCI. Please first enable the repo using StyleCI\'s web inteface.', $path));
                }
            })
            ->success()->get();

        $output->writeln(\sprintf('<info>[DEBUG]</info> Analysis request accepted with status token %s.', $submission->getToken()->getValue()), OutputInterface::VERBOSITY_VERBOSE);

        return Some::create($submission);
    }

    /**
     * Fetch the analysis results.
     */
    private static function fetchAnalysis(
        OutputInterface $output,
        Analyzer $analyzer,
        Submission $submission
    ): Analysis {
        $status = Status::createPending();

        $output->writeln('<info>[INFO]</info>  The analysis is pending.');

        $tries = 0;

        do {
            if ($tries >= 3) {
                \usleep($tries < 10 ? 500000 : 2000000);
            }

            $analysis = $analyzer->getAnalysisDetails($submission)
                ->mapError(function (string $error): void {
                    switch ($error) {
                        case Analyzer::BLOCK_ERROR:
                            throw new RuntimeException('StyleCI has rejected the analysis request either because your IP address has been blocked or because StyleCI is not available in your country. Please contact support for a resolution.');
                        case Analyzer::RATE_ERROR:
                            throw new RuntimeException('StyleCI has rejected the analysis request because your IP address has been rate limited. Please retry the analysis in around one minute\'s time.');
                        default:
                            throw new RuntimeException('We were unable to fetch the analysis status from StyleCI. Please contact support if this error persits.');
                    }
                })
                ->success()->get();

            if (Status::PENDING === $analysis->getStatus()->getValue()) {
                $output->writeln('<info>[DEBUG]</info> The analysis is still pending.', OutputInterface::VERBOSITY_VERY_VERBOSE);
            } elseif (Status::PENDING === $status->getValue()) {
                $output->writeln('<info>[INFO]</info>  The analysis is running.');
            } else {
                $output->writeln('<info>[DEBUG]</info> The analysis is still running.', OutputInterface::VERBOSITY_VERY_VERBOSE);
            }

            $status = $analysis->getStatus();

            $tries++;
        } while (\in_array($analysis->getStatus()->getValue(), [Status::PENDING, Status::RUNNING], true));

        $output->writeln('<info>[INFO]</info>  The analysis is complete.');

        $output->writeln('');

        return $analysis;
    }

    /**
     * Process the analysis results.
     *
     * If dry run is enabled, then will not actually apply any fixes.
     */
    private static function processAnalysis(
        OutputInterface $output,
        Repository $repository,
        Analysis $analysis,
        Repo $repo,
        bool $dryRun
    ): void {
        try {
            switch ($analysis->getStatus()->getValue()) {
                case Status::PASSED:
                    $output->writeln('<info>[INFO]</info>  The analysis has completed with no recommendations.');
                    break;
                case Status::CS_ISSUES:
                    $output->writeln('<info>[INFO]</info>  The analysis has detected code style violations.');
                    break;
                case Status::SYNTAX_ISSUES:
                    $output->writeln('<info>[INFO]</info>  The analysis has detected files with invalid syntax.');
                    break;
                case Status::BOTH_ISSUES:
                    $output->writeln('<info>[INFO]</info>  The analysis has detected both code style violations and files with invalid syntax.');
                    break;
                case Status::CONFIG_ERROR:
                    $output->writeln('<info>[ERROR]</info>  The analysis was unable to complete due to a configuration issue.');

                    throw new RuntimeException('There was a configuration issue.'.\implode('', \array_map(function (Message $message): string {
                        return \sprintf("\n\n%s\n%s", $message->getTitle(), $message->getBody());
                    }, $analysis->getMessages())));
                case Status::ACCESS_ERROR:
                    $output->writeln('<info>[ERROR]</info>  The analysis was unable to complete due to a problem accessing the code.');

                    throw new RuntimeException("There was a problem accessing the code.\n");
                case Status::TIMEOUT_ERROR:
                    $output->writeln('<info>[ERROR]</info>  The analysis was unable to complete due to a platform timeout.');

                    throw new RuntimeException("There was a platform timeout.\n");
                default:
                    $output->writeln('<info>[ERROR]</info>  The analysis was unable to complete due to a platform error.');

                    throw new RuntimeException("There was a platform error.\n");
            }
        } finally {
            $output->writeln(\sprintf('<info>[INFO]</info>  The results are also available at <href=%s>%s</>.', $analysis->getUrl()->getValue(), $analysis->getUrl()->getValue()));
        }

        $analysis->getChanges()->map(function (Changes $changes) use ($output, $repository, $repo, $dryRun, $analysis): void {
            $analysis->getPatch()->map(function (Patch $patch) use ($output, $repository, $repo, $dryRun, $changes): void {
                $output->writeln('');

                if ($dryRun) {
                    $output->writeln(\sprintf('<info>[INFO]</info>  The fixes to apply to %d %s are...', $changes->getValue(), 1 === $changes->getValue() ? 'file' : 'files'));
                    $output->writeln('');
                    $output->writeln($patch->getValue(), OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_RAW);
                    $output->writeln('');
                } else {
                    $output->write(\sprintf('<info>[INFO]</info>  Applying fixes to %d %s...', $changes->getValue(), 1 === $changes->getValue() ? 'file' : 'files'));
                    $repository->applyPatchLocally($repo, $patch);
                    $output->writeln(' DONE!');
                    $output->writeln('<info>[DEBUG]</info> The fixes applied were:', OutputInterface::VERBOSITY_VERY_VERBOSE);
                    $output->writeln('', OutputInterface::VERBOSITY_VERY_VERBOSE);
                    $output->writeln($patch->getValue(), OutputInterface::VERBOSITY_VERY_VERBOSE | OutputInterface::OUTPUT_RAW);
                    $output->writeln('', OutputInterface::VERBOSITY_VERY_VERBOSE);
                }
            });
        });

        if (\in_array($analysis->getStatus()->getValue(), [Status::SYNTAX_ISSUES, Status::BOTH_ISSUES], true)) {
            throw new RuntimeException('StyleCI has detected syntax errors in your code.'.\implode('', \array_map(function (Message $message): string {
                return \sprintf("\n\n%s\n%s", $message->getTitle(), $message->getBody());
            }, $analysis->getMessages())));
        }
    }
}
