<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Console\Util;

use StyleCI\CLI\Exception\ExceptionInterface;
use Symfony\Component\Console\Exception\ExceptionInterface as ConsoleException;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

final class CommandExecutor
{
    /**
     * Execute the given command callable.
     *
     * @param callable(InputInterface, OutputInterface): void $callable
     */
    public static function execute(InputInterface $input, OutputInterface $output, string $header, callable $callable): int
    {
        $output->writeln($header);
        $output->writeln('');

        try {
            $callable($input, $output);
        } catch (ConsoleException $e) {
            throw $e;
        } catch (ExceptionInterface $e) {
            throw new RuntimeException($e->getMessage());
        } catch (Throwable $e) {
            throw new RuntimeException('An internal error has occured! Please contact support.', 0, $e);
        }

        return 0;
    }
}
