<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Contract;

use GrahamCampbell\ResultType\Result;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Diff;
use StyleCI\CLI\Model\Remote;
use StyleCI\CLI\Model\State;
use StyleCI\CLI\Model\Submission;

interface Analyzer
{
    /**
     * The noauth error constant.
     *
     * @var string
     */
    public const NOAUTH_ERROR = 'noauth';

    /**
     * The denied error constant.
     *
     * @var string
     */
    public const UNAUTH_ERROR = 'unauth';

    /**
     * The upgrade error constant.
     *
     * @var string
     */
    public const UPGRADE_ERROR = 'upgrade';

    /**
     * The suspended error constant.
     *
     * @var string
     */
    public const SUSPENDED_ERROR = 'suspended';

    /**
     * The pro error constant.
     *
     * @var string
     */
    public const PRO_ERROR = 'pro';

    /**
     * The block error constant.
     *
     * @var string
     */
    public const BLOCK_ERROR = 'block';

    /**
     * The denied error constant.
     *
     * @var string
     */
    public const DENIED_ERROR = 'denied';

    /**
     * The rate error constant.
     *
     * @var string
     */
    public const RATE_ERROR = 'rate';

    /**
     * The large error constant.
     *
     * @var string
     */
    public const LARGE_ERROR = 'large';

    /**
     * The analysis error constant.
     *
     * @var string
     */
    public const ANALYSIS_ERROR = 'analysis';

    /**
     * Submit a given known state and changes for analysis.
     *
     * @throws \RuntimeException
     *
     * @return \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Submission,string>
     */
    public function submitForAnalysis(Auth $auth, Remote $remote, State $state, Diff $changes = null, bool $onlyChangedFiles = false): Result;

    /**
     * Get the analysis details.
     *
     * @throws \RuntimeException
     *
     * @return \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Analysis,string>
     */
    public function getAnalysisDetails(Submission $submission): Result;
}
