<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Contract;

use PhpOption\Option;
use StyleCI\CLI\Model\Commit;
use StyleCI\CLI\Model\Patch;
use StyleCI\CLI\Model\Repo;

interface Repository
{
    /**
     * Get the main remote of a repository.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Remote>
     */
    public function getMainRemote(Repo $repo): Option;

    /**
     * Get the latest remote state of a repository.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\State>
     */
    public function getLatestRemoteState(Repo $repo): Option;

    /**
     * Generate a local diff from a given commit.
     *
     * The option will be unset if there are no local changes from the given
     * commit. Any other kind of error will result in a runtime exception.
     *
     * @throws \RuntimeException
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Diff>
     */
    public function generateLocalDiff(Repo $repo, Commit $commit): Option;

    /**
     * Apply the given patch to the repository.
     *
     * If we are unable to apply the patch, for whatever reason, expected, or
     * unexpected, a runtime exception will be thrown.
     *
     * @throws \RuntimeException
     */
    public function applyPatchLocally(Repo $repo, Patch $patch): void;
}
