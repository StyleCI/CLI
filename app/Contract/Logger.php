<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Contract;

use StyleCI\CLI\Model\Entry;

interface Logger
{
    /**
     * Write an entry to the logs.
     */
    public function write(Entry $entry): void;
}
