<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Contract;

use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Config;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Key;

interface Configuration
{
    /**
     * Clear the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function clearBitbucketKey(): void;

    /**
     * Clear the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitHubKey(): void;

    /**
     * Clear the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitLabKey(): void;

    /**
     * Clear the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function clearGitBinary(): void;

    /**
     * Set the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function setBitbucketKey(Key $bitbucketKey): void;

    /**
     * Set the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function setGitHubKey(Key $gitHubKey): void;

    /**
     * Set the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function setGitLabKey(Key $gitLabKey): void;

    /**
     * Set the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function setGitBinary(Filepath $gitBinary): void;

    /**
     * Get the authentication config.
     *
     * @throws \RuntimeException
     */
    public function getAuthConfig(): Auth;

    /**
     * Get the local analysis config.
     *
     * @throws \RuntimeException
     */
    public function getLocalConfig(): Config;
}
