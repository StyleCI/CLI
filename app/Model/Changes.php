<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Changes
{
    /**
     * @var int
     */
    private $value;

    /**
     * @return void
     */
    private function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @throws \InvalidArgumentException
     */
    public static function create(int $value): self
    {
        if ($value <= 0) {
            throw new \InvalidArgumentException('The value must be strictly positive.');
        }

        return new self($value);
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
