<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

use PhpOption\Option;

final class Auth
{
    /**
     * @var \StyleCI\CLI\Model\Key|null
     */
    private $bitbucketKey;

    /**
     * @var \StyleCI\CLI\Model\Key|null
     */
    private $gitHubKey;

    /**
     * @var \StyleCI\CLI\Model\Key|null
     */
    private $gitLabKey;

    /**
     * @param \StyleCI\CLI\Model\Key|null $bitbucketKey
     * @param \StyleCI\CLI\Model\Key|null $gitHubKey
     * @param \StyleCI\CLI\Model\Key|null $gitLabKey
     *
     * @return void
     */
    private function __construct(Key $bitbucketKey = null, Key $gitHubKey = null, Key $gitLabKey = null)
    {
        $this->bitbucketKey = $bitbucketKey;
        $this->gitHubKey = $gitHubKey;
        $this->gitLabKey = $gitLabKey;
    }

    public static function create(): self
    {
        return new self();
    }

    /**
     * @param \StyleCI\CLI\Model\Key $bitbucketKey
     */
    public function withBitbucketKey(Key $bitbucketKey): self
    {
        return new self($bitbucketKey, $this->gitHubKey, $this->gitLabKey);
    }

    /**
     * @param \StyleCI\CLI\Model\Key $gitHubKey
     */
    public function withGitHubKey(Key $gitHubKey): self
    {
        return new self($this->bitbucketKey, $gitHubKey, $this->gitLabKey);
    }

    /**
     * @param \StyleCI\CLI\Model\Key $gitLabKey
     */
    public function withGitLabKey(Key $gitLabKey): self
    {
        return new self($this->bitbucketKey, $this->gitHubKey, $gitLabKey);
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Key>
     */
    public function getBitbucketKey(): Option
    {
        /** @var Option<Key> */
        return Option::fromValue($this->bitbucketKey);
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Key>
     */
    public function getGitHubKey(): Option
    {
        /** @var Option<Key> */
        return Option::fromValue($this->gitHubKey);
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Key>
     */
    public function getGitLabKey(): Option
    {
        /** @var Option<Key> */
        return Option::fromValue($this->gitLabKey);
    }
}
