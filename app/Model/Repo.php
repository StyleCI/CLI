<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Repo
{
    /**
     * @var \StyleCI\CLI\Model\Filepath
     */
    private $filepath;

    /**
     * @var \StyleCI\CLI\Model\RemoteName
     */
    private $remoteName;

    /**
     * @param \StyleCI\CLI\Model\Filepath   $filepath
     * @param \StyleCI\CLI\Model\RemoteName $remoteName
     *
     * @return void
     */
    private function __construct(Filepath $filepath, RemoteName $remoteName)
    {
        $this->filepath = $filepath;
        $this->remoteName = $remoteName;
    }

    /**
     * @param \StyleCI\CLI\Model\Filepath   $filepath
     * @param \StyleCI\CLI\Model\RemoteName $remoteName
     */
    public static function create(Filepath $filepath, RemoteName $remoteName): self
    {
        return new self($filepath, $remoteName);
    }

    /**
     * @return \StyleCI\CLI\Model\Filepath
     */
    public function getFilepath(): Filepath
    {
        return $this->filepath;
    }

    /**
     * @return \StyleCI\CLI\Model\RemoteName
     */
    public function getRemoteName(): RemoteName
    {
        return $this->remoteName;
    }
}
