<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class State
{
    /**
     * @var \StyleCI\CLI\Model\Branch
     */
    private $branch;

    /**
     * @var \StyleCI\CLI\Model\Commit
     */
    private $commit;

    /**
     * @param \StyleCI\CLI\Model\Branch $branch
     * @param \StyleCI\CLI\Model\Commit $commit
     *
     * @return void
     */
    private function __construct(Branch $branch, Commit $commit)
    {
        $this->branch = $branch;
        $this->commit = $commit;
    }

    /**
     * @param \StyleCI\CLI\Model\Branch $branch
     * @param \StyleCI\CLI\Model\Commit $commit
     */
    public static function create(Branch $branch, Commit $commit): self
    {
        return new self($branch, $commit);
    }

    /**
     * @return \StyleCI\CLI\Model\Branch
     */
    public function getBranch(): Branch
    {
        return $this->branch;
    }

    /**
     * @return \StyleCI\CLI\Model\Commit
     */
    public function getCommit(): Commit
    {
        return $this->commit;
    }
}
