<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

use PhpOption\Option;

final class Config
{
    /**
     * @var \StyleCI\CLI\Model\Filepath|null
     */
    private $repoDirectory;

    /**
     * @var \StyleCI\CLI\Model\OnlyChanged|null
     */
    private $onlyChanged;

    /**
     * @var \StyleCI\CLI\Model\DryRun|null
     */
    private $dryRun;

    /**
     * @var \StyleCI\CLI\Model\RemoteName|null
     */
    private $remoteName;

    /**
     * @var \StyleCI\CLI\Model\Filepath|null
     */
    private $gitBinary;

    /**
     * @param \StyleCI\CLI\Model\Filepath|null    $repoDirectory
     * @param \StyleCI\CLI\Model\OnlyChanged|null $onlyChanged
     * @param \StyleCI\CLI\Model\DryRun|null      $dryRun
     * @param \StyleCI\CLI\Model\RemoteName|null  $remoteName
     * @param \StyleCI\CLI\Model\Filepath|null    $gitBinary
     *
     * @return void
     */
    private function __construct(
        Filepath $repoDirectory = null,
        OnlyChanged $onlyChanged = null,
        DryRun $dryRun = null,
        RemoteName $remoteName = null,
        Filepath $gitBinary = null
    ) {
        $this->repoDirectory = $repoDirectory;
        $this->onlyChanged = $onlyChanged;
        $this->dryRun = $dryRun;
        $this->remoteName = $remoteName;
        $this->gitBinary = $gitBinary;
    }

    public static function create(): self
    {
        return new self();
    }

    /**
     * @param \StyleCI\CLI\Model\Filepath $repoDirectory
     */
    public function withRepoDirectory(Filepath $repoDirectory): self
    {
        return new self(
            $repoDirectory,
            $this->onlyChanged,
            $this->dryRun,
            $this->remoteName,
            $this->gitBinary
        );
    }

    /**
     * @param \StyleCI\CLI\Model\OnlyChanged $onlyChanged
     */
    public function withOnlyChanged(OnlyChanged $onlyChanged): self
    {
        return new self(
            $this->repoDirectory,
            $onlyChanged,
            $this->dryRun,
            $this->remoteName,
            $this->gitBinary
        );
    }

    /**
     * @param \StyleCI\CLI\Model\DryRun $dryRun
     */
    public function withDryRun(DryRun $dryRun): self
    {
        return new self(
            $this->repoDirectory,
            $this->onlyChanged,
            $dryRun,
            $this->remoteName,
            $this->gitBinary
        );
    }

    /**
     * @param \StyleCI\CLI\Model\RemoteName $remoteName
     */
    public function withRemoteName(RemoteName $remoteName): self
    {
        return new self(
            $this->repoDirectory,
            $this->onlyChanged,
            $this->dryRun,
            $remoteName,
            $this->gitBinary
        );
    }

    /**
     * @param \StyleCI\CLI\Model\Filepath $gitBinary
     */
    public function withGitBinary(Filepath $gitBinary): self
    {
        return new self(
            $this->repoDirectory,
            $this->onlyChanged,
            $this->dryRun,
            $this->remoteName,
            $gitBinary
        );
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Filepath>
     */
    public function getRepoDirectory(): Option
    {
        /** @var Option<Filepath> */
        return Option::fromValue($this->repoDirectory);
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\OnlyChanged>
     */
    public function getOnlyChanged(): Option
    {
        /** @var Option<OnlyChanged> */
        return Option::fromValue($this->onlyChanged);
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\DryRun>
     */
    public function getDryRun(): Option
    {
        /** @var Option<DryRun> */
        return Option::fromValue($this->dryRun);
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\RemoteName>
     */
    public function getRemoteName(): Option
    {
        /** @var Option<RemoteName> */
        return Option::fromValue($this->remoteName);
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Filepath>
     */
    public function getGitBinary(): Option
    {
        /** @var Option<Filepath> */
        return Option::fromValue($this->gitBinary);
    }
}
