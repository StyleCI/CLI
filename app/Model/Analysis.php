<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

use PhpOption\Option;

final class Analysis
{
    /**
     * @var \StyleCI\CLI\Model\Url
     */
    private $url;

    /**
     * @var \StyleCI\CLI\Model\Status
     */
    private $status;

    /**
     * @var \StyleCI\CLI\Model\Message[]
     */
    private $messages;

    /**
     * @var \StyleCI\CLI\Model\Changes|null
     */
    private $changes;

    /**
     * @var \StyleCI\CLI\Model\Patch|null
     */
    private $patch;

    /**
     * @param \StyleCI\CLI\Model\Url          $url
     * @param \StyleCI\CLI\Model\Status       $status
     * @param \StyleCI\CLI\Model\Message[]    $messages
     * @param \StyleCI\CLI\Model\Changes|null $changes
     * @param \StyleCI\CLI\Model\Patch|null   $patch
     *
     * @return void
     */
    private function __construct(Url $url, Status $status, array $messages = [], Changes $changes = null, Patch $patch = null)
    {
        $this->url = $url;
        $this->status = $status;
        $this->messages = $messages;
        $this->changes = $changes;
        $this->patch = $patch;
    }

    /**
     * @param \StyleCI\CLI\Model\Url $url
     */
    public static function createPending(Url $url): self
    {
        return new self($url, Status::createPending());
    }

    /**
     * @param \StyleCI\CLI\Model\Url $url
     */
    public static function createRunning(Url $url): self
    {
        return new self($url, Status::createRunning());
    }

    /**
     * @param \StyleCI\CLI\Model\Url $url
     */
    public static function createPassed(Url $url): self
    {
        return new self($url, Status::createPassed());
    }

    /**
     * @param \StyleCI\CLI\Model\Url     $url
     * @param \StyleCI\CLI\Model\Changes $changes
     * @param \StyleCI\CLI\Model\Patch   $patch
     */
    public static function createCsIssues(Url $url, Changes $changes, Patch $patch): self
    {
        return new self($url, Status::createCsIssues(), [], $changes, $patch);
    }

    /**
     * @param \StyleCI\CLI\Model\Url       $url
     * @param \StyleCI\CLI\Model\Message[] $messages
     */
    public static function createSyntaxIssues(Url $url, array $messages): self
    {
        if (0 === \count($messages)) {
            throw new \InvalidArgumentException('There must be at least one message.');
        }

        return new self($url, Status::createSyntaxIssues(), $messages);
    }

    /**
     * @param \StyleCI\CLI\Model\Url       $url
     * @param \StyleCI\CLI\Model\Message[] $messages
     * @param \StyleCI\CLI\Model\Changes   $changes
     * @param \StyleCI\CLI\Model\Patch     $patch
     *
     * @throws \InvalidArgumentException
     */
    public static function createBothIssues(Url $url, array $messages, Changes $changes, Patch $patch): self
    {
        if (0 === \count($messages)) {
            throw new \InvalidArgumentException('There must be at least one message.');
        }

        return new self($url, Status::createBothIssues(), $messages, $changes, $patch);
    }

    /**
     * @param \StyleCI\CLI\Model\Url       $url
     * @param \StyleCI\CLI\Model\Message[] $messages
     *
     * @throws \InvalidArgumentException
     */
    public static function createConfigError(Url $url, array $messages): self
    {
        if (0 === \count($messages)) {
            throw new \InvalidArgumentException('There must be at least one message.');
        }

        return new self($url, Status::createConfigError(), $messages);
    }

    /**
     * @param \StyleCI\CLI\Model\Url $url
     */
    public static function createAccessError(Url $url): self
    {
        return new self($url, Status::createAccessError());
    }

    /**
     * @param \StyleCI\CLI\Model\Url $url
     */
    public static function createTimeoutError(Url $url): self
    {
        return new self($url, Status::createTimeoutError());
    }

    /**
     * @param \StyleCI\CLI\Model\Url $url
     */
    public static function createInternalError(Url $url): self
    {
        return new self($url, Status::createInternalError());
    }

    /**
     * @return \StyleCI\CLI\Model\Url
     */
    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * @return \StyleCI\CLI\Model\Status
     */
    public function getStatus(): Status
    {
        return $this->status;
    }

    /**
     * @return \StyleCI\CLI\Model\Message[]
     */
    public function getMessages(): array
    {
        return $this->messages;
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Changes>
     */
    public function getChanges(): Option
    {
        /** @var \PhpOption\Option<\StyleCI\CLI\Model\Changes> */
        return Option::fromValue($this->changes);
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Patch>
     */
    public function getPatch(): Option
    {
        /** @var \PhpOption\Option<\StyleCI\CLI\Model\Patch> */
        return Option::fromValue($this->patch);
    }
}
