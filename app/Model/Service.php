<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Service
{
    /**
     * The bitbucket service.
     *
     * @var string
     */
    public const BITBUCKET = 'bitbucket';

    /**
     * The github service.
     *
     * @var string
     */
    public const GITHUB = 'github';

    /**
     * The gitlib service.
     *
     * @var string
     */
    public const GITLAB = 'gitlab';

    /**
     * @var string
     */
    private $value;

    /**
     * @return void
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    public static function createBitbucket(): self
    {
        return new self(self::BITBUCKET);
    }

    public static function createGitHub(): self
    {
        return new self(self::GITHUB);
    }

    public static function createGitLab(): self
    {
        return new self(self::GITLAB);
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
