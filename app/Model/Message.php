<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Message
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $body;

    /**
     * @return void
     */
    private function __construct(string $title, string $body)
    {
        $this->title = $title;
        $this->body = $body;
    }

    /**
     * @throws \InvalidArgumentException
     */
    public static function create(string $title, string $body): self
    {
        if ('' === $title) {
            throw new \InvalidArgumentException('The title must be non-empty.');
        }

        if ('' === $body) {
            throw new \InvalidArgumentException('The body must be non-empty.');
        }

        return new self($title, $body);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getBody(): string
    {
        return $this->body;
    }
}
