<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Remote
{
    /**
     * @var \StyleCI\CLI\Model\Host
     */
    private $host;

    /**
     * @var \StyleCI\CLI\Model\Path
     */
    private $path;

    /**
     * @param \StyleCI\CLI\Model\Host $host
     * @param \StyleCI\CLI\Model\Path $path
     *
     * @return void
     */
    private function __construct(Host $host, Path $path)
    {
        $this->host = $host;
        $this->path = $path;
    }

    /**
     * @param \StyleCI\CLI\Model\Host $host
     * @param \StyleCI\CLI\Model\Path $path
     */
    public static function create(Host $host, Path $path): self
    {
        return new self($host, $path);
    }

    /**
     * @return \StyleCI\CLI\Model\Host
     */
    public function getHost(): Host
    {
        return $this->host;
    }

    /**
     * @return \StyleCI\CLI\Model\Path
     */
    public function getPath(): Path
    {
        return $this->path;
    }
}
