<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Status
{
    /**
     * The pending status.
     *
     * @var int
     */
    public const PENDING = 0;

    /**
     * The running status.
     *
     * @var int
     */
    public const RUNNING = 1;

    /**
     * The passed status.
     *
     * @var int
     */
    public const PASSED = 2;

    /**
     * The cs issues status.
     *
     * @var int
     */
    public const CS_ISSUES = 3;

    /**
     * The syntax issues status.
     *
     * @var int
     */
    public const SYNTAX_ISSUES = 4;

    /**
     * The both cs and syntax issues status.
     *
     * @var int
     */
    public const BOTH_ISSUES = 5;

    /**
     * The configuration error status.
     *
     * @var int
     */
    public const CONFIG_ERROR = 6;

    /**
     * The git access error status.
     *
     * @var int
     */
    public const ACCESS_ERROR = 7;

    /**
     * The analysis timeout error status.
     *
     * @var int
     */
    public const TIMEOUT_ERROR = 8;

    /**
     * The other internal error status.
     *
     * @var int
     */
    public const INTERNAL_ERROR = 9;

    /**
     * @var int
     */
    private $value;

    /**
     * @return void
     */
    private function __construct(int $value)
    {
        $this->value = $value;
    }

    public static function createPending(): self
    {
        return new self(self::PENDING);
    }

    public static function createRunning(): self
    {
        return new self(self::RUNNING);
    }

    public static function createPassed(): self
    {
        return new self(self::PASSED);
    }

    public static function createCsIssues(): self
    {
        return new self(self::CS_ISSUES);
    }

    public static function createSyntaxIssues(): self
    {
        return new self(self::SYNTAX_ISSUES);
    }

    public static function createBothIssues(): self
    {
        return new self(self::BOTH_ISSUES);
    }

    public static function createConfigError(): self
    {
        return new self(self::CONFIG_ERROR);
    }

    public static function createAccessError(): self
    {
        return new self(self::ACCESS_ERROR);
    }

    public static function createTimeoutError(): self
    {
        return new self(self::TIMEOUT_ERROR);
    }

    public static function createInternalError(): self
    {
        return new self(self::INTERNAL_ERROR);
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
