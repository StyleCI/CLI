<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class RemoteName
{
    /**
     * @var string
     */
    private $value;

    /**
     * @return void
     */
    private function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @throws \InvalidArgumentException
     */
    public static function create(string $value): self
    {
        if ('' === $value) {
            throw new \InvalidArgumentException('The value must be non-empty.');
        }

        return new self($value);
    }

    /**
     * @throws \InvalidArgumentException
     */
    public static function createDefault(): self
    {
        return new self('origin');
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
