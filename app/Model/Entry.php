<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Entry
{
    /**
     * @var string
     */
    private $value;

    /**
     * @var bool
     */
    private $debug;

    /**
     * @return void
     */
    private function __construct(string $value, bool $debug)
    {
        $this->value = $value;
        $this->debug = $debug;
    }

    /**
     * @throws \InvalidArgumentException
     */
    public static function create(string $value): self
    {
        if ('' === $value) {
            throw new \InvalidArgumentException('The value must be non-empty.');
        }

        return new self($value, false);
    }

    /**
     * @throws \InvalidArgumentException
     */
    public static function createDebug(string $value): self
    {
        if ('' === $value) {
            throw new \InvalidArgumentException('The value must be non-empty.');
        }

        return new self($value, true);
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getDebug(): bool
    {
        return $this->debug;
    }
}
