<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class OnlyChanged
{
    /**
     * @var bool
     */
    private $value;

    /**
     * @return void
     */
    private function __construct(bool $value)
    {
        $this->value = $value;
    }

    public static function create(bool $value): self
    {
        return new self($value);
    }

    public static function createDefault(): self
    {
        return new self(false);
    }

    public function getValue(): bool
    {
        return $this->value;
    }
}
