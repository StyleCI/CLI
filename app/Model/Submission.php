<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Submission
{
    /**
     * @var \StyleCI\CLI\Model\Key
     */
    private $key;

    /**
     * @var \StyleCI\CLI\Model\Token
     */
    private $token;

    /**
     * @param \StyleCI\CLI\Model\Key   $key
     * @param \StyleCI\CLI\Model\Token $token
     *
     * @return void
     */
    private function __construct(Key $key, Token $token)
    {
        $this->key = $key;
        $this->token = $token;
    }

    /**
     * @param \StyleCI\CLI\Model\Key   $key
     * @param \StyleCI\CLI\Model\Token $token
     */
    public static function create(Key $key, Token $token): self
    {
        return new self($key, $token);
    }

    /**
     * @return \StyleCI\CLI\Model\Key
     */
    public function getKey(): Key
    {
        return $this->key;
    }

    /**
     * @return \StyleCI\CLI\Model\Token
     */
    public function getToken(): Token
    {
        return $this->token;
    }
}
