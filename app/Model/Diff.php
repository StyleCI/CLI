<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Model;

final class Diff
{
    /**
     * @var \StyleCI\CLI\Model\Patch
     */
    private $patch;

    /**
     * @var string[]
     */
    private $files;

    /**
     * @param \StyleCI\CLI\Model\Patch $patch
     * @param string[]                 $files
     *
     * @return void
     */
    private function __construct(Patch $patch, array $files)
    {
        $this->patch = $patch;
        $this->files = $files;
    }

    /**
     * @param \StyleCI\CLI\Model\Patch $patch
     * @param string[]                 $files
     */
    public static function create(Patch $patch, array $files): self
    {
        return new self($patch, $files);
    }

    /**
     * @return \StyleCI\CLI\Model\Patch
     */
    public function getPatch(): Patch
    {
        return $this->patch;
    }

    /**
     * @return string[]
     */
    public function getFiles(): array
    {
        return $this->files;
    }
}
