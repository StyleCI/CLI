<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service;

use GrahamCampbell\ResultType\Error;
use GrahamCampbell\ResultType\Result;
use GrahamCampbell\ResultType\Success;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;
use StyleCI\CLI\Contract\Analyzer;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Diff;
use StyleCI\CLI\Model\Key;
use StyleCI\CLI\Model\Remote;
use StyleCI\CLI\Model\Service;
use StyleCI\CLI\Model\State;
use StyleCI\CLI\Model\Submission;
use StyleCI\CLI\Service\Util\KeyFactory;
use StyleCI\CLI\Service\Util\ResponseParser;
use StyleCI\CLI\Service\Util\ServiceFactory;

final class HttpAnalyzer implements Analyzer
{
    /**
     * @var \GuzzleHttp\ClientInterface
     */
    private $guzzle;

    /**
     * Create a new analyzer instance.
     *
     * @return void
     */
    public function __construct(ClientInterface $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * Submit a given known state and changes for analysis.
     *
     * @throws \RuntimeException
     *
     * @return \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Submission,string>
     */
    public function submitForAnalysis(Auth $auth, Remote $remote, State $state, Diff $changes = null, bool $onlyChangedFiles = false): Result
    {
        $path = $remote->getPath();
        $commit = $state->getCommit();
        $branch = $state->getBranch();

        /** @var \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Submission,string> */
        return self::parseService($remote)->flatMap(function (Service $service) use ($auth, $path, $commit, $branch, $changes, $onlyChangedFiles): Result {
            $data = [
                'repo'  => ['service' => $service->getValue(), 'path' => $path->getValue()],
                'state' => ['commit' => $commit->getValue(), 'branch' => $branch->getValue()],
            ];

            if (null !== $changes) {
                $data['changes'] = ['diff' => \base64_encode($changes->getPatch()->getValue()), 'only' => $onlyChangedFiles ? $changes->getFiles() : false];
            }

            /** @var \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Submission,string> */
            return self::locateKey($auth, $service)->flatMap(function (Key $key) use ($data): Result {
                return $this->submitAnalysisRequest($key, $data);
            });
        });
    }

    /**
     * Extract the service from the given remote.
     *
     * @return \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Service,string>
     */
    private static function parseService(Remote $remote): Result
    {
        /** @var \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Service,string> */
        return ServiceFactory::create($remote->getHost())->map(function (Service $service): Result {
            return Success::create($service);
        })->getOrElse(Error::create(Analyzer::ANALYSIS_ERROR));
    }

    /**
     * Locate the required key.
     *
     * @return \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Key,string>
     */
    private static function locateKey(Auth $auth, Service $service): Result
    {
        /** @var \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Key,string> */
        return KeyFactory::create($auth, $service)->map(function (Key $key): Result {
            return Success::create($key);
        })->getOrElse(Error::create(Analyzer::NOAUTH_ERROR));
    }

    /**
     * Submit the given analysis request.
     *
     * @param array{repo: array{service: string, path: string}, state: array{commit: string, branch: string}} $data
     *
     * @throws \RuntimeException
     *
     * @return \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Submission,string>
     */
    private function submitAnalysisRequest(Key $key, array $data): Result
    {
        try {
            $response = $this->guzzle->request('POST', '/analyze', [
                'headers' => ['X-AUTH-KEY' => $key->getValue()],
                'json'    => $data,
                'timeout' => 65,
            ]);
        } catch (ClientException $e) {
            /** @var \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Submission,string> */
            return Error::create(self::getClientExceptionErrorType($e)->getOrThrow($e));
        }

        $token = ResponseParser::parseToken($response)->getOrCall(function (): void {
            throw new \RuntimeException('The server response did not satsify our schema.');
        });

        /** @var \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Submission,string> */
        return Success::create(Submission::create($key, $token));
    }

    /**
     * Get the analysis details.
     *
     * @throws \RuntimeException
     *
     * @return \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Analysis,string>
     */
    public function getAnalysisDetails(Submission $submission): Result
    {
        try {
            $response = $this->guzzle->request('GET', \sprintf('/analyses/%s', $submission->getToken()->getValue()), [
                'headers' => ['X-AUTH-KEY' => $submission->getKey()->getValue()],
            ]);
        } catch (ClientException $e) {
            /** @var \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Analysis,string> */
            return Error::create(self::getClientExceptionErrorType($e)->getOrThrow($e));
        }

        $analysis = ResponseParser::parseAnalysis($response)->getOrCall(function (): void {
            throw new \RuntimeException('The server response did not satsify our schema.');
        });

        /** @var \GrahamCampbell\ResultType\Result<\StyleCI\CLI\Model\Analysis,string> */
        return Success::create($analysis);
    }

    /**
     * Get the client exception error type.
     *
     * @return \PhpOption\Option<string>
     */
    private static function getClientExceptionErrorType(ClientException $exception): Option
    {
        $response = $exception->getResponse();
        $statusCode = $response->getStatusCode();

        if (400 === $statusCode) {
            return Some::create(Analyzer::ANALYSIS_ERROR);
        }

        if (401 === $statusCode) {
            return Some::create(Analyzer::UNAUTH_ERROR);
        }

        if (403 === $statusCode) {
            $body = (string) $response->getBody();

            if (false !== \strpos($body, 'CLI tool version too old. Upgrade required.')) {
                return Some::create(Analyzer::UPGRADE_ERROR);
            }

            if (false !== \strpos($body, 'The account associated to the repo is suspended.')) {
                return Some::create(Analyzer::SUSPENDED_ERROR);
            }

            if (false !== \strpos($body, 'CLI tool analyses not included in your subscription.')) {
                return Some::create(Analyzer::PRO_ERROR);
            }

            if ('error code:' === \substr($body, 0, 11) || false !== \strpos($body, 'Cloudflare')) {
                return Some::create(Analyzer::BLOCK_ERROR);
            }

            return Some::create(Analyzer::DENIED_ERROR);
        }

        if (413 === $statusCode) {
            return Some::create(Analyzer::LARGE_ERROR);
        }

        if (429 === $statusCode) {
            return Some::create(Analyzer::RATE_ERROR);
        }

        return None::create();
    }
}
