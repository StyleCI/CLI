<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service;

use PhpOption\Option;
use StyleCI\CLI\Contract\Configuration;
use StyleCI\CLI\Exception\InvalidConfigurationException;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Config;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Key;
use StyleCI\CLI\Model\RemoteName;
use StyleCI\CLI\Model\Service;
use StyleCI\CLI\Service\Util\Env;

final class EnvConfiguration implements Configuration
{
    /**
     * Clear the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function clearBitbucketKey(): void
    {
        // do nothing
    }

    /**
     * Clear the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitHubKey(): void
    {
        // do nothing
    }

    /**
     * Clear the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitLabKey(): void
    {
        // do nothing
    }

    /**
     * Clear the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function clearGitBinary(): void
    {
        // do nothing
    }

    /**
     * Set the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function setBitbucketKey(Key $bitbucketKey): void
    {
        // do nothing
    }

    /**
     * Set the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function setGitHubKey(Key $gitHubKey): void
    {
        // do nothing
    }

    /**
     * Set the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function setGitLabKey(Key $gitLabKey): void
    {
        // do nothing
    }

    /**
     * Set the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function setGitBinary(Filepath $gitBinary): void
    {
        // do nothing
    }

    /**
     * Get the authentication config.
     *
     * @throws \RuntimeException
     */
    public function getAuthConfig(): Auth
    {
        $auth = Auth::create();

        $auth = self::getApiKey(Service::createBitbucket())
            ->map([$auth, 'withBitbucketKey'])
            ->getOrElse($auth);

        $auth = self::getApiKey(Service::createGitHub())
            ->map([$auth, 'withGitHubKey'])
            ->getOrElse($auth);

        return self::getApiKey(Service::createGitLab())
            ->map([$auth, 'withGitLabKey'])
            ->getOrElse($auth);
    }

    /**
     * Get the named API key.
     *
     * @throws \StyleCI\CLI\Exception\InvalidConfigurationException
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Key>
     */
    private static function getApiKey(Service $service): Option
    {
        $varName = \sprintf('STYLECI_%s_API_KEY', \strtoupper($service->getValue()));

        return Env::getFiltered($varName)
            ->map(function (string $apiKey) use ($varName): Key {
                if (40 !== \strlen($apiKey)) {
                    throw new InvalidConfigurationException(\sprintf('The "%s" environment variable was set but was not the correct length. StyleCI API keys must be 40 characters long.', $varName));
                }

                return Key::create($apiKey);
            });
    }

    /**
     * Get the local analysis config.
     *
     * @throws \RuntimeException
     */
    public function getLocalConfig(): Config
    {
        $config = Config::create();

        $config = Env::getFiltered('STYLECI_REMOTE_NAME')
            ->map([RemoteName::class, 'create'])
            ->map([$config, 'withRemoteName'])
            ->getOrElse($config);

        return Env::getFiltered('STYLECI_GIT_BINARY')
            ->map([Filepath::class, 'create'])
            ->map([$config, 'withGitBinary'])
            ->getOrElse($config);
    }
}
