<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service;

use PhpOption\Option;
use StyleCI\CLI\Contract\Configuration;
use StyleCI\CLI\Exception\InvalidConfigurationException;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Config;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Key;
use StyleCI\CLI\Model\Service;
use StyleCI\CLI\Service\Util\Arr;
use StyleCI\CLI\Service\Util\JsonFile;

final class FileConfiguration implements Configuration
{
    /**
     * @var string
     */
    private $filepath;

    /**
     * @var array<string,mixed>|null
     */
    private $configCache;

    /**
     * Create a new file configuration instance.
     *
     * @return void
     */
    public function __construct(Filepath $configFilepath)
    {
        $this->filepath = $configFilepath->getValue();
    }

    /**
     * Clear the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function clearBitbucketKey(): void
    {
        $this->clearAuthConfigEntry(Service::createBitbucket());
    }

    /**
     * Clear the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitHubKey(): void
    {
        $this->clearAuthConfigEntry(Service::createGitHub());
    }

    /**
     * Clear the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitLabKey(): void
    {
        $this->clearAuthConfigEntry(Service::createGitLab());
    }

    /**
     * Clear the given auth config entry.
     *
     * @throws \RuntimeException
     */
    private function clearAuthConfigEntry(Service $service): void
    {
        $key = \sprintf('auth.%s', $service->getValue());

        /** @var array<string, mixed> */
        $data = Arr::remove($this->readConfigFile(), $key);

        JsonFile::set($this->filepath, $data);

        $this->configCache = $data;
    }

    /**
     * Clear the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function clearGitBinary(): void
    {
        /** @var array<string, mixed> */
        $data = Arr::remove($this->readConfigFile(), 'git.binary');

        JsonFile::set($this->filepath, $data);

        $this->configCache = $data;
    }

    /**
     * Set the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function setBitbucketKey(Key $bitbucketKey): void
    {
        $this->setAuthConfigEntry(Service::createBitbucket(), $bitbucketKey);
    }

    /**
     * Set the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function setGitHubKey(Key $gitHubKey): void
    {
        $this->setAuthConfigEntry(Service::createGitHub(), $gitHubKey);
    }

    /**
     * Set the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function setGitLabKey(Key $gitLabKey): void
    {
        $this->setAuthConfigEntry(Service::createGitLab(), $gitLabKey);
    }

    /**
     * Set the given auth config entry.
     *
     * @throws \RuntimeException
     */
    private function setAuthConfigEntry(Service $service, Key $apiKey): void
    {
        $key = \sprintf('auth.%s', $service->getValue());

        /** @var array<string, mixed> */
        $data = Arr::set($this->readConfigFile(), $key, $apiKey->getValue());

        JsonFile::set($this->filepath, $data);

        $this->configCache = $data;
    }

    /**
     * Set the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function setGitBinary(Filepath $gitBinary): void
    {
        /** @var array<string, mixed> */
        $data = Arr::set($this->readConfigFile(), 'git.binary', $gitBinary->getValue());

        JsonFile::set($this->filepath, $data);

        $this->configCache = $data;
    }

    /**
     * Get the authentication config.
     *
     * @throws \RuntimeException
     */
    public function getAuthConfig(): Auth
    {
        $data = $this->readConfigFile();

        $auth = Auth::create();

        $auth = self::getApiKey($data, Service::createBitbucket())
            ->map([$auth, 'withBitbucketKey'])
            ->getOrElse($auth);

        $auth = self::getApiKey($data, Service::createGitHub())
            ->map([$auth, 'withGitHubKey'])
            ->getOrElse($auth);

        return self::getApiKey($data, Service::createGitLab())
            ->map([$auth, 'withGitLabKey'])
            ->getOrElse($auth);
    }

    /**
     * Get the named auth config entry.
     *
     * @param array<string, mixed> $data
     *
     * @throws \RuntimeException
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Key>
     */
    private static function getApiKey(array $data, Service $service): Option
    {
        $varName = \sprintf('auth.%s', $service->getValue());

        /** @var Option<string> */
        $apiKey = Arr::get($data, $varName)->filter('is_string');

        return $apiKey
            ->map(function (string $apiKey) use ($varName): Key {
                if (40 !== \strlen($apiKey)) {
                    throw new InvalidConfigurationException(\sprintf('The "%s" configuration file entry was set but was not the correct length. StyleCI API keys must be 40 characters long.', $varName));
                }

                return Key::create($apiKey);
            });
    }

    /**
     * Get the local analysis config.
     *
     * @throws \RuntimeException
     */
    public function getLocalConfig(): Config
    {
        $data = $this->readConfigFile();

        $config = Config::create();

        return self::getGitBinary($data)
            ->map([$config, 'withGitBinary'])
            ->getOrElse($config);
    }

    /**
     * Get the git binary filepath entry.
     *
     * @param array<string, mixed> $data
     *
     * @throws \RuntimeException
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Filepath>
     */
    private static function getGitBinary(array $data): Option
    {
        /** @var Option<string> */
        $gitBinary = Arr::get($data, 'git.binary')->filter('is_string');

        return $gitBinary
            ->map(function (string $gitBinary): Filepath {
                if ('' === $gitBinary) {
                    throw new InvalidConfigurationException('The "git.binary" configuration file entry was set but was not the correct length. Git binary filepaths must be non-empty.');
                }

                return Filepath::create($gitBinary);
            });
    }

    /**
     * Get the JSON data of the config file.
     *
     * @throws \RuntimeException
     *
     * @return array<string,mixed>
     */
    private function readConfigFile(): array
    {
        if (null === $this->configCache) {
            $this->configCache = JsonFile::get($this->filepath);
        }

        return  $this->configCache;
    }
}
