<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service;

use GrahamCampbell\GitWrapper\GitWrapper;
use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;
use StyleCI\CLI\Contract\Repository;
use StyleCI\CLI\Model\Branch;
use StyleCI\CLI\Model\Commit;
use StyleCI\CLI\Model\Diff;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Patch;
use StyleCI\CLI\Model\Repo;
use StyleCI\CLI\Model\State;
use StyleCI\CLI\Service\Util\RemoteFactory;

final class GitRepository implements Repository
{
    /**
     * @var \GrahamCampbell\GitWrapper\GitWrapper
     */
    private $gitWrapper;

    /**
     * @var \StyleCI\CLI\Model\Filepath
     */
    private $tempDirectory;

    /**
     * Create a new repository instance.
     *
     * @return void
     */
    public function __construct(GitWrapper $gitWrapper, Filepath $tempDirectory)
    {
        $this->gitWrapper = $gitWrapper;
        $this->tempDirectory = $tempDirectory;
    }

    /**
     * Get the main remote of a repository.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Remote>
     */
    public function getMainRemote(Repo $repo): Option
    {
        try {
            $remote = $this->gitWrapper->git(
                \sprintf('remote get-url %s', $repo->getRemoteName()->getValue()),
                $repo->getFilepath()->getValue()
            );
        } catch (\RuntimeException $e) {
            return None::create();
        }

        return RemoteFactory::create(\trim($remote));
    }

    /**
     * Get the latest remote state of a repository.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\State>
     */
    public function getLatestRemoteState(Repo $repo): Option
    {
        try {
            $log = $this->gitWrapper->git(
                'log -200 --pretty=format:%H',
                $repo->getFilepath()->getValue()
            );
        } catch (\RuntimeException $e) {
            return None::create();
        }

        foreach (\explode("\n", \trim($log)) as $commit) {
            $commit = Commit::create($commit);
            $branch = $this->getRemoteBranch($repo, $commit);
            if ($branch->isDefined()) {
                return Some::create(State::create($branch->get(), $commit));
            }
        }

        return None::create();
    }

    /**
     * Get the remote branch containing the given commit, if it exists.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Branch>
     */
    private function getRemoteBranch(Repo $repo, Commit $commit): Option
    {
        try {
            $refs = $this->gitWrapper->git(
                \sprintf('branch -r --contains %s', $commit->getValue()),
                $repo->getFilepath()->getValue()
            );
        } catch (\RuntimeException $e) {
            return None::create();
        }

        $remotePrefix = \sprintf('%s/', $repo->getRemoteName()->getValue());
        $prefixLength = \strlen($remotePrefix);

        foreach (\explode("\n", \trim($refs)) as $ref) {
            if (false !== ($pos = \strpos($ref, $remotePrefix)) && 0 !== \strpos($branch = \trim(\substr($ref, $pos + $prefixLength)), 'HEAD')) {
                return Some::create(Branch::create($branch));
            }
        }

        return None::create();
    }

    /**
     * Generate a local diff from a given commit.
     *
     * The option will be unset if there are no local changes from the given
     * commit. Any other kind of error will result in a runtime exception.
     *
     * @throws \RuntimeException
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Diff>
     */
    public function generateLocalDiff(Repo $repo, Commit $commit): Option
    {
        $dir = $repo->getFilepath()->getValue();
        $ref = $commit->getValue();

        try {
            $patch = $this->getBinaryPatch($dir, $ref);

            if ('' === $patch) {
                return None::create();
            }

            $files = $this->getChangedFiles($dir, $ref);
        } catch (\RuntimeException $e) {
            return None::create();
        }

        return Some::create(Diff::create(Patch::create($patch), $files));
    }

    /**
     * Generate a binary patch.
     *
     * @throws \RuntimeException
     */
    private function getBinaryPatch(string $dir, string $ref): string
    {
        $command = self::buildDiffCommand(\sprintf('%s --src-prefix=a/ --dst-prefix=b/ --binary --minimal', $ref));

        return $this->gitWrapper->git($command, $dir);
    }

    /**
     * Generate a list of changed files.
     *
     * @throws \RuntimeException
     *
     * @return string[]
     */
    private function getChangedFiles(string $dir, string $ref): array
    {
        $command = self::buildDiffCommand(\sprintf('%s --name-only --diff-filter=d', $ref));

        return \explode("\n", \trim($this->gitWrapper->git($command, $dir)));
    }

    /**
     * Build a git diff command.
     */
    private static function buildDiffCommand(string $options): string
    {
        return \sprintf('diff %s --ignore-submodules --no-ext-diff --no-textconv', $options);
    }

    /**
     * Apply the given patch to the repository.
     *
     * If we are unable to apply the patch, for whatever reason, expected, or
     * unexpected, a runtime exception will be thrown.
     *
     * @throws \RuntimeException
     */
    public function applyPatchLocally(Repo $repo, Patch $patch): void
    {
        $file = $this->tempDirectory->getValue().'/'.\bin2hex(\random_bytes(20)).'.patch';

        try {
            if (false === @\file_put_contents($file, $patch->getValue())) {
                throw new \RuntimeException('Unable to write temp patch file.');
            }

            $dir = $repo->getFilepath()->getValue();
            $this->gitWrapper->git('stage .', $dir);
            $this->gitWrapper->workingCopy($dir)->apply($file, ['whitespace' => 'nowarn', '3way' => true]);
        } finally {
            @\unlink($file);
        }
    }
}
