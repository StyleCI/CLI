<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service;

use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Model\Entry;
use Symfony\Component\Console\Output\OutputInterface;

final class ConsoleLogger implements Logger
{
    /**
     * @var \Symfony\Component\Console\Output\OutputInterface
     */
    private $output;

    /**
     * Create a new logger instance.
     *
     * @return void
     */
    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * Create a new logger using the given output.
     */
    public static function create(OutputInterface $output): self
    {
        return new self($output);
    }

    /**
     * Write an entry to the logs.
     */
    public function write(Entry $entry): void
    {
        if ($entry->getDebug()) {
            $this->output->writeln(\sprintf('<info>[DEBUG]</info> %s', $entry->getValue()), OutputInterface::VERBOSITY_DEBUG);
            $this->output->writeln('', OutputInterface::VERBOSITY_DEBUG);
        } else {
            $this->output->writeln(\sprintf('<info>[DEBUG]</info> %s', $entry->getValue()), OutputInterface::VERBOSITY_VERY_VERBOSE);
        }
    }
}
