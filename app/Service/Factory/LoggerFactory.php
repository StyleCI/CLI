<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Factory;

use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Service\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

final class LoggerFactory
{
    /**
     * Create a new logger instance.
     */
    public static function create(OutputInterface $output): Logger
    {
        return new ConsoleLogger($output);
    }
}
