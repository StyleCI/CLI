<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Factory;

use GrahamCampbell\GuzzleFactory\GuzzleFactory;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use StyleCI\CLI\Contract\Analyzer;
use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Model\Entry;
use StyleCI\CLI\Model\Host;
use StyleCI\CLI\Model\UserAgent;
use StyleCI\CLI\Service\HttpAnalyzer;
use StyleCI\CLI\Service\Util\PsrLogger;

final class AnalyzerFactory
{
    /**
     * Create a new analyzer instance.
     */
    public static function create(Host $host, UserAgent $userAgent, Logger $logger = null): Analyzer
    {
        $options = [
            'base_uri' => self::createBaseUri($host),
            'handler'  => self::createHandler($logger),
            'headers'  => self::createHeaders($userAgent),
        ];

        return new HttpAnalyzer(GuzzleFactory::make($options));
    }

    /**
     * Create the Guzzle base URI.
     */
    private static function createBaseUri(Host $host): string
    {
        return \sprintf('https://%s', $host->getValue());
    }

    /**
     * Create a new Guzzle handler stack.
     */
    private static function createHandler(?Logger $logger): HandlerStack
    {
        $handler = HandlerStack::create();

        if (null !== $logger) {
            $handler->push(self::createLogMiddleware(function (string $message) use ($logger): void {
                $logger->write(Entry::createDebug(\sprintf("HTTP RESPONSE:\n%s", \rtrim($message))));
            }, '{response}'));

            $handler->push(self::createLogMiddleware(function (string $message) use ($logger): void {
                $logger->write(Entry::createDebug(\sprintf("HTTP REQUEST:\n%s", \rtrim($message))));
            }, '{request}'));
        }

        return $handler;
    }

    /**
     * @param callable(string): void $logger
     *
     * @return callable(callable): callable
     */
    private static function createLogMiddleware(callable $logger, string $format): callable
    {
        /** @var callable(callable): callable */
        return Middleware::log(new PsrLogger($logger), new MessageFormatter($format));
    }

    /**
     * @return array<string,string>
     */
    private static function createHeaders(UserAgent $userAgent): array
    {
        return ['Accept-Encoding' => 'gzip', 'User-Agent' => $userAgent->getValue()];
    }
}
