<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Factory;

use StyleCI\CLI\Contract\Configuration;
use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Service\ConsoleConfiguration;
use StyleCI\CLI\Service\EnvConfiguration;
use StyleCI\CLI\Service\FileConfiguration;
use StyleCI\CLI\Service\LoggingConfiguration;
use StyleCI\CLI\Service\MergingConfiguration;
use Symfony\Component\Console\Input\InputInterface;

final class ConfigurationFactory
{
    /**
     * Create a new configuration instance.
     */
    public static function create(Filepath $configFilepath, Filepath $workingDirectory, InputInterface $input, Logger $logger = null): Configuration
    {
        $configuration = self::createInner($configFilepath, $workingDirectory, $input);

        return null === $logger ? $configuration : new LoggingConfiguration($configuration, $logger);
    }

    private static function createInner(Filepath $configFilepath, Filepath $workingDirectory, InputInterface $input): Configuration
    {
        return new MergingConfiguration([
            new FileConfiguration($configFilepath),
            new EnvConfiguration(),
            new ConsoleConfiguration($workingDirectory, $input),
        ]);
    }
}
