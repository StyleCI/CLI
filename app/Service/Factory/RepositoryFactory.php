<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Factory;

use GrahamCampbell\GitWrapper\GitWrapper;
use StyleCI\CLI\Contract\Repository;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Service\GitRepository;

final class RepositoryFactory
{
    /**
     * Create a new repository instance.
     */
    public static function create(Filepath $tempDirectory, Filepath $gitBinary = null): Repository
    {
        return new GitRepository(self::createGitWrapper($gitBinary), $tempDirectory);
    }

    /**
     * Create a new git wrapper instance.
     */
    private static function createGitWrapper(?Filepath $gitBinary): GitWrapper
    {
        return new GitWrapper(
            null !== $gitBinary ? $gitBinary->getValue() : null
        );
    }
}
