<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service;

use PhpOption\Option;
use StyleCI\CLI\Contract\Configuration;
use StyleCI\CLI\Contract\Logger;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Config;
use StyleCI\CLI\Model\Entry;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Key;
use StyleCI\CLI\Model\RemoteName;

final class LoggingConfiguration implements Configuration
{
    /**
     * @var \StyleCI\CLI\Contract\Configuration
     */
    private $configuration;

    /**
     * @var \StyleCI\CLI\Contract\Logger
     */
    private $logger;

    /**
     * Create a new logging configuration instance.
     *
     * @return void
     */
    public function __construct(Configuration $configuration, Logger $logger)
    {
        $this->configuration = $configuration;
        $this->logger = $logger;
    }

    /**
     * Clear the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function clearBitbucketKey(): void
    {
        $this->configuration->clearBitbucketKey();
    }

    /**
     * Clear the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitHubKey(): void
    {
        $this->configuration->clearGitHubKey();
    }

    /**
     * Clear the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitLabKey(): void
    {
        $this->configuration->clearGitLabKey();
    }

    /**
     * Clear the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function clearGitBinary(): void
    {
        $this->configuration->clearGitBinary();
    }

    /**
     * Set the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function setBitbucketKey(Key $bitbucketKey): void
    {
        $this->configuration->setBitbucketKey($bitbucketKey);
    }

    /**
     * Set the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function setGitHubKey(Key $gitHubKey): void
    {
        $this->configuration->setGitHubKey($gitHubKey);
    }

    /**
     * Set the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function setGitLabKey(Key $gitLabKey): void
    {
        $this->configuration->setGitLabKey($gitLabKey);
    }

    /**
     * Set the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function setGitBinary(Filepath $gitBinary): void
    {
        $this->configuration->setGitBinary($gitBinary);
    }

    /**
     * Get the authentication config.
     *
     * @throws \RuntimeException
     */
    public function getAuthConfig(): Auth
    {
        $auth = $this->configuration->getAuthConfig();

        $this->logApiKey($auth->getBitbucketKey(), 'Bitbucket');
        $this->logApiKey($auth->getGitHubKey(), 'GitHub');
        $this->logApiKey($auth->getGitLabKey(), 'GitLab');

        return $auth;
    }

    /**
     * Log the given API key to the console.
     *
     * @param \PhpOption\Option<\StyleCI\CLI\Model\Key> $apiKey
     */
    private function logApiKey(Option $apiKey, string $serviceName): void
    {
        $key = $apiKey->map(function (Key $key): string {
            return $key->getValue();
        })->getOrElse('');

        $this->logger->write(Entry::create(\sprintf('Using the StyleCI for %s API key "%s".', $serviceName, $key)));
    }

    /**
     * Get the local analysis config.
     *
     * @throws \RuntimeException
     */
    public function getLocalConfig(): Config
    {
        $config = $this->configuration->getLocalConfig();

        $config->getRepoDirectory()->forAll(function (Filepath $repoDirectory): void {
            $this->logger->write(Entry::create(\sprintf('Using the repository directory "%s".', $repoDirectory->getValue())));
        });

        $config->getRemoteName()->forAll(function (RemoteName $remoteName): void {
            $this->logger->write(Entry::create(\sprintf('Using the custom remote name "%s".', $remoteName->getValue())));
        });

        $config->getGitBinary()->forAll(function (Filepath $gitBinary): void {
            $this->logger->write(Entry::create(\sprintf('Using the custom git binary "%s".', $gitBinary->getValue())));
        });

        return $config;
    }
}
