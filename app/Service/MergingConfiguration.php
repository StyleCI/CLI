<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service;

use StyleCI\CLI\Contract\Configuration;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Config;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Key;

final class MergingConfiguration implements Configuration
{
    /**
     * @var \StyleCI\CLI\Contract\Configuration[]
     */
    private $configurations;

    /**
     * Create a new merging configuration instance.
     *
     * @param \StyleCI\CLI\Contract\Configuration[] $configurations
     *
     * @return void
     */
    public function __construct(array $configurations)
    {
        $this->configurations = $configurations;
    }

    /**
     * Clear the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function clearBitbucketKey(): void
    {
        foreach ($this->configurations as $configuration) {
            $configuration->clearBitbucketKey();
        }
    }

    /**
     * Clear the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitHubKey(): void
    {
        foreach ($this->configurations as $configuration) {
            $configuration->clearGitHubKey();
        }
    }

    /**
     * Clear the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitLabKey(): void
    {
        foreach ($this->configurations as $configuration) {
            $configuration->clearGitLabKey();
        }
    }

    /**
     * Clear the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function clearGitBinary(): void
    {
        foreach ($this->configurations as $configuration) {
            $configuration->clearGitBinary();
        }
    }

    /**
     * Set the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function setBitbucketKey(Key $bitbucketKey): void
    {
        foreach ($this->configurations as $configuration) {
            $configuration->setBitbucketKey($bitbucketKey);
        }
    }

    /**
     * Set the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function setGitHubKey(Key $gitHubKey): void
    {
        foreach ($this->configurations as $configuration) {
            $configuration->setGitHubKey($gitHubKey);
        }
    }

    /**
     * Set the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function setGitLabKey(Key $gitLabKey): void
    {
        foreach ($this->configurations as $configuration) {
            $configuration->setGitLabKey($gitLabKey);
        }
    }

    /**
     * Set the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function setGitBinary(Filepath $gitBinary): void
    {
        foreach ($this->configurations as $configuration) {
            $configuration->setGitBinary($gitBinary);
        }
    }

    /**
     * Get the authentication config.
     *
     * @throws \RuntimeException
     */
    public function getAuthConfig(): Auth
    {
        $auth = Auth::create();

        foreach ($this->configurations as $configuration) {
            $result = $configuration->getAuthConfig();

            $auth = $result->getBitbucketKey()
                ->map([$auth, 'withBitbucketKey'])
                ->getOrElse($auth);

            $auth = $result->getGitHubKey()
                ->map([$auth, 'withGitHubKey'])
                ->getOrElse($auth);

            $auth = $result->getGitLabKey()
                ->map([$auth, 'withGitLabKey'])
                ->getOrElse($auth);
        }

        return $auth;
    }

    /**
     * Get the local analysis config.
     *
     * @throws \RuntimeException
     */
    public function getLocalConfig(): Config
    {
        $config = Config::create();

        foreach ($this->configurations as $configuration) {
            $result = $configuration->getLocalConfig();

            $config = $result->getRepoDirectory()
                ->map([$config, 'withRepoDirectory'])
                ->getOrElse($config);

            $config = $result->getOnlyChanged()
                ->map([$config, 'withOnlyChanged'])
                ->getOrElse($config);

            $config = $result->getDryRun()
                ->map([$config, 'withDryRun'])
                ->getOrElse($config);

            $config = $result->getRemoteName()
                ->map([$config, 'withRemoteName'])
                ->getOrElse($config);

            $config = $result->getGitBinary()
                ->map([$config, 'withGitBinary'])
                ->getOrElse($config);
        }

        return $config;
    }
}
