<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service;

use PhpOption\Option;
use PhpOption\Some;
use StyleCI\CLI\Contract\Configuration;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Config;
use StyleCI\CLI\Model\DryRun;
use StyleCI\CLI\Model\Filepath;
use StyleCI\CLI\Model\Key;
use StyleCI\CLI\Model\OnlyChanged;
use StyleCI\CLI\Model\RemoteName;
use Symfony\Component\Console\Input\InputInterface;

final class ConsoleConfiguration implements Configuration
{
    /**
     * @var string
     */
    private $workingDirectory;

    /**
     * @var \Symfony\Component\Console\Input\InputInterface
     */
    private $input;

    /**
     * Create a new console configuration instance.
     *
     * @return void
     */
    public function __construct(Filepath $workingDirectory, InputInterface $input)
    {
        $this->workingDirectory = $workingDirectory->getValue();
        $this->input = $input;
    }

    /**
     * Clear the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function clearBitbucketKey(): void
    {
        // do nothing
    }

    /**
     * Clear the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitHubKey(): void
    {
        // do nothing
    }

    /**
     * Clear the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function clearGitLabKey(): void
    {
        // do nothing
    }

    /**
     * Clear the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function clearGitBinary(): void
    {
        // do nothing
    }

    /**
     * Set the Bitbucket API key.
     *
     * @throws \RuntimeException
     */
    public function setBitbucketKey(Key $bitbucketKey): void
    {
        // do nothing
    }

    /**
     * Set the GitHub API key.
     *
     * @throws \RuntimeException
     */
    public function setGitHubKey(Key $gitHubKey): void
    {
        // do nothing
    }

    /**
     * Set the GitLab API key.
     *
     * @throws \RuntimeException
     */
    public function setGitLabKey(Key $gitLabKey): void
    {
        // do nothing
    }

    /**
     * Set the git binary filepath.
     *
     * @throws \RuntimeException
     */
    public function setGitBinary(Filepath $gitBinary): void
    {
        // do nothing
    }

    /**
     * Get the authentication config.
     *
     * @throws \RuntimeException
     */
    public function getAuthConfig(): Auth
    {
        return Auth::create();
    }

    /**
     * Get the local analysis config.
     *
     * @throws \RuntimeException
     */
    public function getLocalConfig(): Config
    {
        $config = Config::create();

        $config = $this->getRepoDirectory()
            ->map([$config, 'withRepoDirectory'])
            ->getOrElse($config);

        $config = $this->getOnlyChanged()
            ->map([$config, 'withOnlyChanged'])
            ->getOrElse($config);

        $config = $this->getDryRun()
            ->map([$config, 'withDryRun'])
            ->getOrElse($config);

        $config = $this->getRemoteName()
            ->map([$config, 'withRemoteName'])
            ->getOrElse($config);

        return $this->getGitBinary()
            ->map([$config, 'withGitBinary'])
            ->getOrElse($config);
    }

    /**
     * Get the repository directory.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Filepath>
     */
    private function getRepoDirectory(): Option
    {
        /** @var string */
        $repoDirectory = $this->input->getArgument('directory') ?? '';

        if ('' === $repoDirectory) {
            $repoDirectory = $this->workingDirectory;
        } elseif ('/' === \substr($repoDirectory, 0, 1)) {
            $repoDirectory = \sprintf('%s/%s', $this->workingDirectory, $repoDirectory);
        }

        return Some::create($repoDirectory)
            ->map([Filepath::class, 'create']);
    }

    /**
     * Get the only changed config.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\OnlyChanged>
     */
    private function getOnlyChanged(): Option
    {
        $onlyChanged = false !== $this->input->getOption('only-changed');

        return Some::create($onlyChanged)
            ->map([OnlyChanged::class, 'create']);
    }

    /**
     * Get the dry run mode config.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\DryRun>
     */
    private function getDryRun(): Option
    {
        $dryRun = false !== $this->input->getOption('dry-run');

        return Some::create($dryRun)
            ->map([DryRun::class, 'create']);
    }

    /**
     * Get the repository remote name.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\RemoteName>
     */
    private function getRemoteName(): Option
    {
        /** @var string */
        $remoteName = $this->input->getOption('remote-name') ?? '';

        return Option::fromValue($remoteName, '')
            ->map([RemoteName::class, 'create']);
    }

    /**
     * Get the git binary filepath.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Filepath>
     */
    private function getGitBinary(): Option
    {
        /** @var string */
        $gitBinary = $this->input->getOption('git-binary') ?? '';

        return Option::fromValue($gitBinary, '')
            ->map([Filepath::class, 'create']);
    }
}
