<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use RuntimeException;

/**
 * @internal
 */
final class JsonFile
{
    /**
     * Get the JSON data of the given file.
     *
     * @throws \RuntimeException
     *
     * @return array<string,mixed>
     */
    public static function get(string $filepath): array
    {
        if (!\file_exists($filepath)) {
            return [];
        }

        $result = @\file_get_contents($filepath);

        if (false === $result) {
            throw new RuntimeException(\sprintf('Unable to read file %s', $filepath));
        }

        /** @var mixed */
        $data = @\json_decode($result, true);

        if (!self::isStringKeyedArray($data)) {
            throw new RuntimeException(\sprintf('Unable to decode file %s', $filepath));
        }

        /** @var array<string,mixed> */
        return $data;
    }

    /**
     * Determine if the data is a string-keyed array.
     *
     * @param mixed $data
     */
    private static function isStringKeyedArray($data): bool
    {
        if (!\is_array($data)) {
            return false;
        }

        foreach (\array_keys($data) as $key) {
            if (!\is_string($key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Set the JSON data of the given file.
     *
     * @param array<string,mixed> $data
     *
     * @throws \RuntimeException
     */
    public static function set(string $filepath, array $data): void
    {
        self::ensureDirectoryExists(\dirname($filepath));

        $result = @\file_put_contents($filepath, \sprintf("%s\n", \json_encode($data, JSON_PRETTY_PRINT)));

        if (false === $result) {
            throw new RuntimeException(\sprintf('Unable to write file %s', $filepath));
        }
    }

    /**
     * Create the given directory if it doesn't already exist.
     *
     * @param string $filepath
     *
     * @throws \RuntimeException
     */
    private static function ensureDirectoryExists($filepath): void
    {
        if (\is_dir($filepath)) {
            return;
        }

        $result = @\mkdir($filepath, 0777, true);

        if (!$result) {
            throw new RuntimeException(\sprintf('Unable to create directory %s', $filepath));
        }
    }
}
