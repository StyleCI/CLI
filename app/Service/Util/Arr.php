<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use PhpOption\Option;
use ValueError;

/**
 * @internal
 */
final class Arr
{
    /**
     * Get an item from an array using dot notation.
     *
     * @param array<int|string,mixed> $array
     *
     * @return \PhpOption\Option<mixed>
     */
    public static function get(array $array, string $key): Option
    {
        $parts = \explode('.', $key, 2);

        if ('' === $parts[0]) {
            throw new ValueError(\sprintf('%s::get(): Argument #2 ($key) had invalid syntax', self::class));
        }

        if (isset($parts[1])) {
            /** @var Option<array> */
            $inner = Option::fromArraysValue($array, $parts[0])->filter('is_array');

            return $inner->flatMap(function (array $array) use ($parts): Option {
                return self::get($array, $parts[1]);
            });
        }

        return Option::fromArraysValue($array, $parts[0]);
    }

    /**
     * Remove an array item to a given value using dot notation.
     *
     * @param array<int|string,mixed> $array
     *
     * @return array<int|string,mixed>
     */
    public static function remove(array $array, string $key): array
    {
        return self::write($array, $key, null);
    }

    /**
     * Set an array item to a given value using dot notation.
     *
     * @param array<int|string,mixed> $array
     * @param mixed                   $value
     *
     * @return array<int|string,mixed>
     */
    public static function set(array $array, string $key, $value): array
    {
        if (null === $value) {
            throw new ValueError(\sprintf('%s::set(): Argument #3 ($value) cannot be null', self::class));
        }

        return self::write($array, $key, $value);
    }

    /**
     * @param array<int|string,mixed> $array
     * @param mixed                   $value
     *
     * @return array<int|string,mixed>
     */
    private static function write(array $array, string $key, $value): array
    {
        /** @var string[] */
        $parts = \explode('.', $key, 2);

        if ('' === $parts[0]) {
            throw new ValueError(\sprintf('%s::write(): Argument #2 ($key) had invalid syntax', self::class));
        }

        if (isset($parts[1])) {
            /** @var mixed */
            $arr = $array[$parts[0]] ?? null;
            $array[$parts[0]] = self::write(\is_array($arr) ? $arr : [], $parts[1], $value);
            \ksort($array);

            return $array;
        }

        if (null === $value) {
            unset($array[$parts[0]]);
        } else {
            /** @var mixed */
            $array[$parts[0]] = $value;
            \ksort($array);
        }

        return $array;
    }
}
