<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;
use StyleCI\CLI\Model\Host;
use StyleCI\CLI\Model\Service;

/**
 * @internal
 */
final class ServiceFactory
{
    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Service>
     */
    public static function create(Host $host): Option
    {
        switch ($host->getValue()) {
            case 'bitbucket.org':
                return Some::create(Service::createBitbucket());
            case 'github.com':
                return Some::create(Service::createGitHub());
            case 'gitlab.com':
                return Some::create(Service::createGitLab());
            default:
                return None::create();
        }
    }
}
