<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use PhpOption\None;
use PhpOption\Option;
use StyleCI\CLI\Model\Auth;
use StyleCI\CLI\Model\Service;

/**
 * @internal
 */
final class KeyFactory
{
    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Key>
     */
    public static function create(Auth $auth, Service $service): Option
    {
        switch ($service->getValue()) {
            case Service::BITBUCKET:
                return $auth->getBitbucketKey();
            case Service::GITHUB:
                return $auth->getGitHubKey();
            case Service::GITLAB:
                return $auth->getGitLabKey();
            default:
                return None::create();
        }
    }
}
