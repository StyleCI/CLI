<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use StyleCI\CLI\Model\Analysis;
use StyleCI\CLI\Model\Token;

/**
 * @internal
 */
final class ResponseParser
{
    /**
     * Create a token from an HTTP response.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Token>
     */
    public static function parseToken(ResponseInterface $response): Option
    {
        return self::decodeJsonBody($response->getBody())->flatMap(function (array $data): Option {
            /** @var array<string|int,mixed> $data */
            return TokenFactory::create($data);
        });
    }

    /**
     * Create an analysis from an HTTP response.
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Analysis>
     */
    public static function parseAnalysis(ResponseInterface $response): Option
    {
        /** @var \PhpOption\Option<\StyleCI\CLI\Model\Analysis> */
        return self::decodeJsonBody($response->getBody())->flatMap(function (array $data): Option {
            if (!isset($data['analysis']) || !\is_array($data['analysis'])) {
                return None::create();
            }

            return AnalysisFactory::create($data['analysis']);
        });
    }

    /**
     * Decode an HTTP response body as JSON.
     *
     * @return \PhpOption\Option<array<string|int,mixed>>
     */
    private static function decodeJsonBody(StreamInterface $body): Option
    {
        /** @var mixed */
        $decoded = @\json_decode((string) $body, true);

        if (!\is_array($decoded) || !isset($decoded['success']) || !\is_array($decoded['success'])) {
            return None::create();
        }

        $data = $decoded['success'];

        foreach (\array_keys($data) as $key) {
            if (!\is_string($key)) {
                return None::create();
            }
        }

        /** @var \PhpOption\Option<array<string|int,mixed>> */
        return Some::create($data);
    }
}
