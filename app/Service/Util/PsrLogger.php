<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use Psr\Log\LoggerInterface;

/**
 * @internal
 */
final class PsrLogger implements LoggerInterface
{
    /**
     * @var callable(string): void
     */
    private $logger;

    /**
     * @param callable(string): void $logger
     *
     * @return void
     */
    public function __construct(callable $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string  $message
     * @param mixed[] $context
     */
    public function emergency($message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }

    /**
     * @param string  $message
     * @param mixed[] $context
     */
    public function alert($message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }

    /**
     * @param string  $message
     * @param mixed[] $context
     */
    public function critical($message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }

    /**
     * @param string  $message
     * @param mixed[] $context
     */
    public function error($message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }

    /**
     * @param string  $message
     * @param mixed[] $context
     */
    public function warning($message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }

    /**
     * @param string  $message
     * @param mixed[] $context
     */
    public function notice($message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }

    /**
     * @param string  $message
     * @param mixed[] $context
     */
    public function info($message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }

    /**
     * @param string  $message
     * @param mixed[] $context
     */
    public function debug($message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }

    /**
     * @param mixed   $level
     * @param string  $message
     * @param mixed[] $context
     */
    public function log($level, $message, array $context = []): void
    {
        \call_user_func($this->logger, $message);
    }
}
