<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;

/**
 * @internal
 */
final class Env
{
    /**
     * Get the value of a environment variable.
     *
     * Returns None if the value is not a string or is unset.
     *
     * @return \PhpOption\Option<string>
     */
    public static function get(string $name): Option
    {
        if (isset($_SERVER[$name]) && \is_string($_SERVER[$name])) {
            return Some::create($_SERVER[$name]);
        }

        return None::create();
    }

    /**
     * Get the value of a non-empty environment variable.
     *
     * Returns None if the value is not a non-empty string or is unset.
     *
     * @return \PhpOption\Option<string>
     */
    public static function getFiltered(string $name): Option
    {
        return self::get($name)->filter(function (string $value): bool {
            return '' !== \trim($value);
        });
    }
}
