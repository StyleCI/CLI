<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;
use StyleCI\CLI\Model\Host;
use StyleCI\CLI\Model\Path;
use StyleCI\CLI\Model\Remote;

/**
 * @internal
 */
final class RemoteFactory
{
    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Remote>
     */
    public static function create(string $remote): Option
    {
        if ('.git' === \substr($remote, -4)) {
            $remote = \substr($remote, 0, -4);
        }

        if ('git@' === \substr($remote, 0, 4)) {
            return self::createSsh(\substr($remote, 4));
        }

        if ('https://' === \substr($remote, 0, 8)) {
            return self::createHttps(\substr($remote, 8));
        }

        return None::create();
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Remote>
     */
    private static function createSsh(string $remote): Option
    {
        $split = \explode(':', $remote, 2);

        if ('' !== $split[0] && isset($split[1]) && '' !== $split[1]) {
            return Some::create(
                Remote::create(Host::create($split[0]), Path::create($split[1]))
            );
        }

        return None::create();
    }

    /**
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Remote>
     */
    private static function createHttps(string $remote): Option
    {
        $split = \explode('/', $remote, 2);

        if ('' !== $split[0] && isset($split[1]) && '' !== $split[1]) {
            $parts = \explode('@', $split[0], 2);

            $host = \end($parts);

            if ('' !== $host) {
                return Some::create(
                    Remote::create(Host::create($host), Path::create($split[1]))
                );
            }
        }

        return None::create();
    }
}
