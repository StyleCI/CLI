<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;
use StyleCI\CLI\Model\Analysis;
use StyleCI\CLI\Model\Changes;
use StyleCI\CLI\Model\Message;
use StyleCI\CLI\Model\Patch;
use StyleCI\CLI\Model\Status;
use StyleCI\CLI\Model\Url;

/**
 * @internal
 */
final class AnalysisFactory
{
    /**
     * @param array<string|int,mixed> $data
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Analysis>
     */
    public static function create(array $data): Option
    {
        /** @var \PhpOption\Option<\StyleCI\CLI\Model\Analysis> */
        return self::createUrl($data)->flatMap(function (Url $url) use ($data): Option {
            /** @var \PhpOption\Option<\StyleCI\CLI\Model\Status> */
            return self::createStatus($data)->flatMap(function (Status $status) use ($data, $url): Option {
                switch ($status->getValue()) {
                    case Status::PENDING:
                        return Some::create(Analysis::createPending($url));
                    case Status::RUNNING:
                        return Some::create(Analysis::createRunning($url));
                    case Status::PASSED:
                        return Some::create(Analysis::createPassed($url));
                    case Status::CS_ISSUES:
                        return self::createChanges($data)->flatMap(function (Changes $changes) use ($data, $url): Option {
                            return self::createPatch($data)->map(function (Patch $patch) use ($url, $changes): Analysis {
                                return Analysis::createCsIssues($url, $changes, $patch);
                            });
                        });
                    case Status::SYNTAX_ISSUES:
                        return self::createMessages($data)->map(function (array $messages) use ($url): Analysis {
                            /** @var \StyleCI\CLI\Model\Message[] $messages */
                            return Analysis::createSyntaxIssues($url, $messages);
                        });
                    case Status::BOTH_ISSUES:
                        return self::createMessages($data)->flatMap(function (array $messages) use ($data, $url): Option {
                            return self::createChanges($data)->flatMap(function (Changes $changes) use ($data, $url, $messages): Option {
                                return self::createPatch($data)->map(function (Patch $patch) use ($url, $messages, $changes): Analysis {
                                    /** @var \StyleCI\CLI\Model\Message[] $messages */
                                    return Analysis::createBothIssues($url, $messages, $changes, $patch);
                                });
                            });
                        });
                    case Status::CONFIG_ERROR:
                        return self::createMessages($data)->map(function (array $messages) use ($url): Analysis {
                            /** @var \StyleCI\CLI\Model\Message[] $messages */
                            return Analysis::createConfigError($url, $messages);
                        });
                    case Status::ACCESS_ERROR:
                        return Some::create(Analysis::createAccessError($url));
                    case Status::TIMEOUT_ERROR:
                        return Some::create(Analysis::createTimeoutError($url));
                    case Status::INTERNAL_ERROR:
                        return Some::create(Analysis::createInternalError($url));
                    default:
                        return None::create();
                }
            });
        });
    }

    /**
     * @param array<string|int,mixed> $data
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Url>
     */
    private static function createUrl(array $data): Option
    {
        if (!isset($data['url']) || !\is_string($data['url']) || 'https://' !== \substr($data['url'], 0, 8)) {
            return None::create();
        }

        return Some::create(Url::create($data['url']));
    }

    /**
     * @param array<string|int,mixed> $data
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Status>
     */
    private static function createStatus(array $data): Option
    {
        if (!isset($data['status']) || !\is_string($data['status'])) {
            return None::create();
        }

        switch ($data['status']) {
            case 'pending':
                return Some::create(Status::createPending());
            case 'running':
                return Some::create(Status::createRunning());
            case 'passed':
                return Some::create(Status::createPassed());
            case 'cs_issues':
                return Some::create(Status::createCsIssues());
            case 'syntax_issues':
                return Some::create(Status::createSyntaxIssues());
            case 'both_issues':
                return Some::create(Status::createBothIssues());
            case 'config_error':
                return Some::create(Status::createConfigError());
            case 'access_error':
                return Some::create(Status::createAccessError());
            case 'timeout_error':
                return Some::create(Status::createTimeoutError());
            case 'internal_error':
                return Some::create(Status::createInternalError());
            default:
                return None::create();
        }
    }

    /**
     * @param array<string|int,mixed> $data
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Message[]>
     */
    private static function createMessages(array $data): Option
    {
        if (!isset($data['messages']) || !\is_array($data['messages'])) {
            return None::create();
        }

        $output = [];

        /** @var mixed $message */
        foreach ($data['messages'] as $message) {
            if (!\is_array($message)) {
                return None::create();
            }

            if (!isset($message['title']) || !\is_string($message['title']) || '' === $message['title']) {
                return None::create();
            }

            if (!isset($message['body']) || !\is_string($message['body']) || '' === $message['body']) {
                return None::create();
            }

            $output[] = Message::create($message['title'], $message['body']);
        }

        if (0 === \count($output)) {
            return None::create();
        }

        /** @var \PhpOption\Option<\StyleCI\CLI\Model\Message[]> */
        return Some::create($output);
    }

    /**
     * @param array<string|int,mixed> $data
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Changes>
     */
    private static function createChanges(array $data): Option
    {
        if (!isset($data['changes']) || !\is_int($data['changes']) || $data['changes'] <= 0) {
            return None::create();
        }

        return Some::create(Changes::create($data['changes']));
    }

    /**
     * @param array<string|int,mixed> $data
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Patch>
     */
    private static function createPatch(array $data): Option
    {
        if (!isset($data['diff']) || !\is_string($data['diff'])) {
            return None::create();
        }

        $patch = @\base64_decode($data['diff'], true);

        if (false === $patch || '' === $patch) {
            return None::create();
        }

        return Some::create(Patch::create($patch));
    }
}
