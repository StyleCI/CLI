<?php

declare(strict_types=1);

/*
 * This file is part of StyleCI CLI.
 *
 * (c) Graham Campbell Technology Ltd
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace StyleCI\CLI\Service\Util;

use PhpOption\None;
use PhpOption\Option;
use PhpOption\Some;
use StyleCI\CLI\Model\Token;

/**
 * @internal
 */
final class TokenFactory
{
    /**
     * @param array<string|int,mixed> $data
     *
     * @return \PhpOption\Option<\StyleCI\CLI\Model\Token>
     */
    public static function create(array $data): Option
    {
        if (!isset($data['token']) || !\is_string($data['token']) || '' === $data['token']) {
            return None::create();
        }

        return Some::create(Token::create($data['token']));
    }
}
